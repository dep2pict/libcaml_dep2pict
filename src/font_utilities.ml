module Font_utilities = struct
  let oc = open_out "/tmp/tmp.svg" in
  close_out oc;
  ignore (Sys.command "chmod 777 /tmp/tmp.svg")

  let surface = Cairo.SVG.create "/tmp/tmp.svg" ~w:1000. ~h:1000.
  let t = Cairo.create surface

  let get_width (text : string) =
    if text = ""
    then 0.
    else let ext = Cairo.text_extents t text in ext.Cairo.width

  let get_text_dimension ?(bold=false) ?(italic=false) fontname size (text : string) =
    if text=""
    then
      begin
        Cairo.set_font_size t (float_of_int size);
        let ext2 = Cairo.font_extents t in
        (0.,ext2.Cairo.ascent,ext2.Cairo.descent)
      end
    else
      begin
        Cairo.select_font_face t fontname
          ~slant:(if italic then Cairo.Italic else Cairo.Upright)
          ~weight:(if bold then Cairo.Bold else Cairo.Normal);
        Cairo.set_font_size t (float_of_int size) ;
        let ext = Cairo.text_extents t text in
        let ext2 = Cairo.font_extents t in
        (ext.Cairo.width,ext2.Cairo.ascent,ext2.Cairo.descent)
      end

  let remove_tmp_file () =
    if Sys.file_exists "/tmp/tmp.svg"
    then Sys.remove "/tmp/tmp.svg"
end
