%{
(** Parseur de fichier au format .dep *)
  open Printf

  open Dep2pict_types
  open Dep2pict_utils
%}

%token EQUAL
%token SEMI
%token BACKGROUND
%token WORD_SIZE
%token SUBWORD_SIZE
%token SUBGROUP_SIZE
%token SUPGROUP_SIZE
%token EDGELABEL_SIZE
%token <string> VALUE
%token SUBWORD
%token SUPWORD
%token FORECOLOR
%token ARROW
%token LABEL
%token COLOR
%token BGCOLOR
%token STYLE
%token EOF
%token GRAPH
%token LEFT_BRACKET
%token RIGHT_BRACKET
%token WORDS
%token HSPACE
%token VSPACE
%token UNORIENTED
%token SUBCOLOR
%token ITALIC
%token BOLD
%token ALIGN
%token LABEL_OFFSET_POS
%token LABEL_OFFSET_NEG
%token OPACITY
%token WORD_SPACING
%token MARGIN_TOP
%token MARGIN_BOTTOM
%token MARGIN_LEFT
%token MARGIN_RIGHT
%token GROUPS
%token LEFT_CROC
%token RIGHT_CROC
%token WORDS_ATT
%token BORDER_COLOR
%token FONTNAME
%token MAINWORD
%token DDOT
%token GR_GRAPH
%token TOP
%token BOTTOM
%token EDGES
%token SCALE
%token LEFT
%token RIGHT
%token <string> INT
%token <string> QUOTED_STRING

%start <Dep2pict_types.graph> main
%%

%public separated_list_final_opt(separator,X):
|                                                               { [] }
|   x=X                                                         { [x] }
|   x=X; separator; xs=separated_list_final_opt(separator,X)    { x :: xs }

%public separated_nonempty_list_final_opt(separator,X):
|   x=X                                                                  { [x] }
|   x=X; separator                                                       { [x] }
|   x=X; separator; xs=separated_nonempty_list_final_opt(separator,X)    { x :: xs }

main: 
        | graph_gr words option(groups) edges EOF   { !Dep2pict_global.graph }
        | graph words edges EOF                     { !Dep2pict_global.graph }

 
/*============================================================================*/
/* [GRAPH] & [GRAPH_GR]                                                       */
/*============================================================================*/

graph_gr: 
        | GR_GRAPH delimited(LEFT_BRACKET, separated_list_final_opt(SEMI, graph_properties), RIGHT_BRACKET)       { !Dep2pict_global.graph.gr_graph <- true; }
        
graph:
        | GRAPH delimited(LEFT_BRACKET, separated_list_final_opt(SEMI, graph_properties), RIGHT_BRACKET)          { !Dep2pict_global.graph.gr_graph <- false;}


/*==================================================*/
/* Graph properties                                 */
/*==================================================*/

graph_properties:
        | BACKGROUND EQUAL v = VALUE                 { !Dep2pict_global.graph.background <- v}
        | BACKGROUND EQUAL v = QUOTED_STRING         { !Dep2pict_global.graph.background <- v}
        
        | VSPACE EQUAL v = INT                       { !Dep2pict_global.graph.vspace <- Dep2pict_utils.fos v}
        | VSPACE EQUAL v = QUOTED_STRING             { !Dep2pict_global.graph.vspace <- Dep2pict_utils.fos v}
        
        | HSPACE EQUAL v = INT                       { !Dep2pict_global.graph.hspace <- Dep2pict_utils.fos v}
        | HSPACE EQUAL v = QUOTED_STRING             { !Dep2pict_global.graph.hspace <- Dep2pict_utils.fos v}
        
        | OPACITY EQUAL v = INT                      { !Dep2pict_global.graph.opacity <- (Dep2pict_utils.foi (Dep2pict_utils.ios v))/.100.}
        | OPACITY EQUAL v = QUOTED_STRING            { !Dep2pict_global.graph.opacity <- (Dep2pict_utils.foi (Dep2pict_utils.ios v))/.100.}
        
        | WORD_SPACING EQUAL v = INT                 { !Dep2pict_global.graph.word_spacing <- (Dep2pict_utils.fos v)}
        | WORD_SPACING EQUAL v = QUOTED_STRING       { !Dep2pict_global.graph.word_spacing <- (Dep2pict_utils.fos v)}
        
        | MARGIN_TOP EQUAL v = INT                   { !Dep2pict_global.graph.margin_top <- (Dep2pict_utils.fos v)}
        | MARGIN_TOP EQUAL v = QUOTED_STRING         { !Dep2pict_global.graph.margin_top <- (Dep2pict_utils.fos v)}
        
        | MARGIN_BOTTOM EQUAL v = INT                { !Dep2pict_global.graph.margin_bottom <- (Dep2pict_utils.fos v)}
        | MARGIN_BOTTOM EQUAL v = QUOTED_STRING      { !Dep2pict_global.graph.margin_bottom <- (Dep2pict_utils.fos v)}
        
        | MARGIN_LEFT EQUAL v = INT                  { !Dep2pict_global.graph.margin_left <- (Dep2pict_utils.fos v)}
        | MARGIN_LEFT EQUAL v = QUOTED_STRING        { !Dep2pict_global.graph.margin_left <- (Dep2pict_utils.fos v)}
        
        | MARGIN_RIGHT EQUAL v = INT                 { !Dep2pict_global.graph.margin_right <- (Dep2pict_utils.fos v)}
        | MARGIN_RIGHT EQUAL v = QUOTED_STRING       { !Dep2pict_global.graph.margin_right <- (Dep2pict_utils.fos v)}
        
        | SUBWORD_SIZE EQUAL v = INT                 { !Dep2pict_global.graph.subword_size <- (Dep2pict_utils.ios v)}
        | SUBWORD_SIZE EQUAL v = QUOTED_STRING       { !Dep2pict_global.graph.subword_size <- (Dep2pict_utils.ios v)}
        
        | SUBGROUP_SIZE EQUAL v = INT                { !Dep2pict_global.graph.subgroup_size <- (Dep2pict_utils.ios v)}
        | SUBGROUP_SIZE EQUAL v = QUOTED_STRING      { !Dep2pict_global.graph.subgroup_size <- (Dep2pict_utils.ios v)}
        
        | SUPGROUP_SIZE EQUAL v = INT                { !Dep2pict_global.graph.supgroup_size <- (Dep2pict_utils.ios v)}
        | SUPGROUP_SIZE EQUAL v = QUOTED_STRING      { !Dep2pict_global.graph.supgroup_size <- (Dep2pict_utils.ios v)}
        
        | EDGELABEL_SIZE EQUAL v = INT               { !Dep2pict_global.graph.edge_label_size <- (Dep2pict_utils.ios v)}
        | EDGELABEL_SIZE EQUAL s = QUOTED_STRING     { !Dep2pict_global.graph.edge_label_size <- (Dep2pict_utils.ios s)}
        
        | FONTNAME EQUAL s = QUOTED_STRING           { !Dep2pict_global.graph.fontname <- s }
        
        | WORD_SIZE EQUAL v = INT                    { !Dep2pict_global.graph.word_size <- Dep2pict_utils.ios v }
        | WORD_SIZE EQUAL v = QUOTED_STRING          { !Dep2pict_global.graph.word_size <- Dep2pict_utils.ios v }
        
        | SCALE EQUAL v = INT                        { !Dep2pict_global.graph.scale <- (Dep2pict_utils.ios v)}
        | SCALE EQUAL v = QUOTED_STRING              { !Dep2pict_global.graph.scale <- (Dep2pict_utils.ios v)}


/*============================================================================*/
/* [GROUPS]                                                                   */
/*============================================================================*/

groups:
        GROUPS delimited(LEFT_BRACKET, list(group), RIGHT_BRACKET)              { }


group:
        | id = VALUE delimited(LEFT_BRACKET, separated_list_final_opt(SEMI, group_properties), RIGHT_BRACKET)
          { 
            !Dep2pict_global.group.gindex <- !Dep2pict_global.index;
            begin 
              try ignore(StringMap.find id !Dep2pict_global.i); raise (Id_already_in_use id)
              with Not_found -> Dep2pict_global.i := StringMap.add id (!Dep2pict_global.group.gindex,G) !Dep2pict_global.i
            end;
            incr Dep2pict_global.index;
            if !Dep2pict_global.graph.gr_graph
            then (!Dep2pict_global.graph.nodes <- Nodes.add (Group !Dep2pict_global.group) !Dep2pict_global.graph.nodes;);

            Dep2pict_global.group := {
              gindex=(-1);
              gid="";
              gx=0.;
              gy=0.;
              gwidth=0.;
              gheight=0.;
              gnodes=[];
              gbordercolor="black";
              gnb_edges_left=0;
              gnb_edges_right=0;
              gindex_x=0;
              gbackground="white";
              gsubword="";
              gsupword="";
            }
          }


/*===================================================*/
/* group properties                                  */
/*===================================================*/

interval:
        | v1 = VALUE DDOT v2 = VALUE      { (v1,v2) }
        | v = VALUE 
          { match Str.split (Str.regexp "\\.\\.") v with
            | [v1; v2] -> (v1,v2)
            | _ -> raise (Syntax_error (sprintf "Illegal words specification \"%s\"" v))
          }
              
group_properties:                                                                                                                               
        | WORDS_ATT EQUAL inter = delimited(LEFT_CROC,interval,RIGHT_CROC)                      
          { 
            if (!Dep2pict_global.graph.gr_graph) 
            then
              begin
                let (f,t) = inter in
                let fi =
                  try fst (StringMap.find f !Dep2pict_global.i);
                  with Not_found -> raise (Cant_find_index f) in
                let ti =
                  try fst (StringMap.find t !Dep2pict_global.i);
                  with Not_found -> raise (Cant_find_index t) in
                if fi > ti
                then raise (Cant_find_index (Printf.sprintf "%s to %s" f t)) 
                else 
                  if ti = fi 
                  then !Dep2pict_global.group.gnodes <- [fi]
                  else
                    begin
                      let nodes = ref [] in
                      for i = fi to ti do
                        nodes := i::(!nodes)
                      done;
                      !Dep2pict_global.group.gnodes <- List.rev !nodes
                    end
              end
          }

        | BORDER_COLOR EQUAL v = VALUE                  { if (!Dep2pict_global.graph.gr_graph) then !Dep2pict_global.group.gbordercolor <- v }
        | BORDER_COLOR EQUAL v = QUOTED_STRING          { if (!Dep2pict_global.graph.gr_graph) then !Dep2pict_global.group.gbordercolor <- v }  
        
        | BACKGROUND EQUAL v = VALUE                    { if (!Dep2pict_global.graph.gr_graph) then !Dep2pict_global.group.gbackground <- v }
        | BACKGROUND EQUAL v = QUOTED_STRING            { if (!Dep2pict_global.graph.gr_graph) then !Dep2pict_global.group.gbackground <- v }
        
        | SUBWORD EQUAL s = QUOTED_STRING               { if (!Dep2pict_global.graph.gr_graph) then !Dep2pict_global.group.gsubword <- s }
        | SUPWORD EQUAL s = QUOTED_STRING               { if (!Dep2pict_global.graph.gr_graph) then !Dep2pict_global.group.gsupword <- s }


/*============================================================================*/
/* [WORDS]                                                                    */
/*============================================================================*/

words:
        | WORDS delimited(LEFT_BRACKET, list(word), RIGHT_BRACKET)        {  }


word:
        | id = VALUE delimited(LEFT_BRACKET, separated_list_final_opt(SEMI, word_properties), RIGHT_BRACKET)

          { 
            !Dep2pict_global.word.windex <- !Dep2pict_global.index;
            begin 
              try 
                ignore(StringMap.find id !Dep2pict_global.i);
                raise (Id_already_in_use id)
              with Not_found ->
                Dep2pict_global.word := {!Dep2pict_global.word with input_id = id};
                Dep2pict_global.i := StringMap.add id (!Dep2pict_global.word.windex,W) !Dep2pict_global.i
            end;
            incr Dep2pict_global.index;
            !Dep2pict_global.graph.nodes <- Nodes.add (Word !Dep2pict_global.word) !Dep2pict_global.graph.nodes;
            Dep2pict_global.word := {
              input_id ="Unset.2";
              windex=(-1);
              wx=0.;
              wy=0.;
              wwidth=0.;
              wheight=0.;
              text="";
              italic=false;
              bold=false;
              subword="";
              forecolor="black";
              subcolor="black";
              wnb_edges_left_top=0;
              wnb_edges_right_top=0;
              wnb_edges_left_bottom=0;
              wnb_edges_right_bottom=0;
              windex_x=0;
            }
          }


/*============================================================================*/
/* words properties                                                           */
/*============================================================================*/

word_properties:
        | MAINWORD EQUAL s = QUOTED_STRING      { !Dep2pict_global.word.text <- s }
        | SUBWORD EQUAL s = QUOTED_STRING       { !Dep2pict_global.word.subword <- s }
                
        | FORECOLOR EQUAL v = VALUE             { !Dep2pict_global.word.forecolor <- v}
        | FORECOLOR EQUAL v = QUOTED_STRING     { !Dep2pict_global.word.forecolor <- v}
        
        | SUBCOLOR EQUAL v = VALUE              { !Dep2pict_global.word.subcolor <- v}
        | SUBCOLOR EQUAL v = QUOTED_STRING      { !Dep2pict_global.word.subcolor <- v}
        
        | ITALIC                                { !Dep2pict_global.word.italic <- true}
        | BOLD                                  { !Dep2pict_global.word.bold <- true}


/*============================================================================*/
/* [EDGES]                                                                    */
/*============================================================================*/

edges:
        EDGES delimited(LEFT_BRACKET,list(edge), RIGHT_BRACKET)  { }



edge:
        | n1 = VALUE ARROW n2 = VALUE delimited(LEFT_BRACKET, separated_list_final_opt(SEMI, edge_properties), RIGHT_BRACKET)
          {
            let first_node =
              try StringMap.find n1 !Dep2pict_global.i;
              with Not_found -> raise (Cant_find_index n1) in
            let second_node =
              try StringMap.find n2 !Dep2pict_global.i;
              with Not_found -> raise (Cant_find_index n2) in
            Dep2pict_global.is_for_words := (snd first_node) = W && (snd second_node) = W;
            if !Dep2pict_global.is_for_words
            then
              begin
                !Dep2pict_global.wedge.edge_id <- Printf.sprintf "%s__%s__%s" n1 n2 !Dep2pict_global.tr.tr_label;

                !Dep2pict_global.wedge.wefrom_node <- fst first_node;
                !Dep2pict_global.wedge.weto_node <- fst second_node;
                !Dep2pict_global.wedge.wecolor <- !Dep2pict_global.tr.tr_color;
                !Dep2pict_global.wedge.weforecolor <- !Dep2pict_global.tr.tr_forecolor;
                !Dep2pict_global.wedge.webgcolor <- !Dep2pict_global.tr.tr_bgcolor;
                !Dep2pict_global.wedge.welabel_align <- !Dep2pict_global.tr.tr_align;
                !Dep2pict_global.wedge.welabel <- !Dep2pict_global.tr.tr_label;
                !Dep2pict_global.wedge.wetop <- !Dep2pict_global.tr.tr_top;
                !Dep2pict_global.wedge.westyle <- !Dep2pict_global.tr.tr_style;
                !Dep2pict_global.wedge.welabel_offset_neg <- !Dep2pict_global.tr.tr_label_offset_neg;
                !Dep2pict_global.wedge.welabel_offset_pos <- !Dep2pict_global.tr.tr_label_offset_pos; 
                !Dep2pict_global.wedge.weoriented <- !Dep2pict_global.tr.tr_oriented;

                if (!Dep2pict_global.wedge.wefrom_node == !Dep2pict_global.wedge.weto_node)
                then (raise (Dep2pict_utils.Loop_in_dependencies (Printf.sprintf "from '%d' to '%d'" !Dep2pict_global.wedge.wefrom_node !Dep2pict_global.wedge.weto_node)));

                !Dep2pict_global.graph.edges <- Edges.add (Word_edge !Dep2pict_global.wedge) !Dep2pict_global.graph.edges;
                Dep2pict_global.wedge := {
                  edge_id = "Unset";
                  wefrom_node = (-1);
                  weto_node = (-1);
                  wecolor = "black";
                  weforecolor = "black";
	          webgcolor = "transparent";
                  westyle="solid";
                  weindex = Edges.cardinal !Dep2pict_global.graph.edges;
                  welevel = 0.;
                  welabel_offset_pos=0.;
                  welabel_offset_neg=0.;
                  welabel_align=0;
                  welabel = "";
                  wetop=true;
                  weoriented=true;
                  wex1 = 0.;
                  wex2 = 0.;
                  wex3 = 0.;
                  wex4 = 0.;
                  wey1 = 0.;
                  wey2 = 0.;
                  wey3 = 0.;
                  wey4 = 0.;
                };
                Dep2pict_global.tr := {
                  Dep2pict_types.tr_align= 0; 
                  tr_label= "";
                  tr_forecolor= "black";
                  tr_color= "black";
                  tr_bgcolor= "transparent";
                  tr_style= "solid";
                  tr_top= true;
                  tr_label_offset_pos= 0.;
                  tr_label_offset_neg= 0.;
                  tr_oriented=true;
                }
              end
            else
              if !Dep2pict_global.graph.gr_graph
              then
                begin
                  !Dep2pict_global.gedge.gedge_id <- Printf.sprintf "%s__%s__%s" n1 n2 !Dep2pict_global.tr.tr_label;

                  !Dep2pict_global.gedge.gefrom_node <- fst first_node;
                  !Dep2pict_global.gedge.geto_node <- fst second_node;
                  !Dep2pict_global.gedge.gecolor <- !Dep2pict_global.tr.tr_color;
                  !Dep2pict_global.gedge.geforecolor <- !Dep2pict_global.tr.tr_forecolor;
                  !Dep2pict_global.gedge.gebgcolor <- !Dep2pict_global.tr.tr_bgcolor;
                  !Dep2pict_global.gedge.gelabel_align <- !Dep2pict_global.tr.tr_align;
                  !Dep2pict_global.gedge.gelabel <- !Dep2pict_global.tr.tr_label;
                  !Dep2pict_global.gedge.gestyle <- !Dep2pict_global.tr.tr_style;
                  !Dep2pict_global.gedge.gelabel_offset_neg <- !Dep2pict_global.tr.tr_label_offset_neg;
                  !Dep2pict_global.gedge.gelabel_offset_pos <- !Dep2pict_global.tr.tr_label_offset_pos; 
                  !Dep2pict_global.gedge.gefrom_node <- fst first_node;
                  !Dep2pict_global.gedge.geto_node <- fst second_node;
                  !Dep2pict_global.gedge.georiented <- !Dep2pict_global.tr.tr_oriented;
                  if (!Dep2pict_global.gedge.gefrom_node == !Dep2pict_global.gedge.geto_node)
                  then (raise (Dep2pict_utils.Loop_in_dependencies (Printf.sprintf "from '%d' to '%d'" !Dep2pict_global.gedge.gefrom_node !Dep2pict_global.gedge.geto_node)));
                  !Dep2pict_global.graph.edges <- Edges.add (Group_edge !Dep2pict_global.gedge) !Dep2pict_global.graph.edges;
                  Dep2pict_global.gedge := {
  gedge_id = "Unset";
                    gefrom_node = (-1);
                    geto_node = (-1);
                    gecolor = "black";
                    geforecolor = "black";
	            gebgcolor = "transparent";
                    geindex = Edges.cardinal !Dep2pict_global.graph.edges;
                    gestyle="solid";
                    gelevel = 0.;
                    gelabel = "";
                    gelabel_offset_pos=0.;
                    gelabel_offset_neg=0.;
                    gelabel_align=0;
                    georiented=true;
                    gex1 = 0.;
                    gex2 = 0.;
                    gex3 = 0.;
                    gex4 = 0.;
                    gey1 = 0.;
                    gey2 = 0.;
                    gey3 = 0.;
                    gey4 = 0.;
                    gefrom_word=false;
                  };
                  Dep2pict_global.tr := {
                    Dep2pict_types.tr_align= 0; 
                    tr_label= "";
                    tr_forecolor= "black";
                    tr_color= "black";
                    tr_bgcolor= "transparent";
                    tr_style= "solid";
                    tr_top= true;
                    tr_label_offset_pos= 0.;
                    tr_label_offset_neg= 0.;
                    tr_oriented=true;
                  }
                end
        }

edge_properties:
        | ALIGN EQUAL LEFT                                 { !Dep2pict_global.tr.tr_align <- 2  }
        
        | ALIGN EQUAL RIGHT                                { !Dep2pict_global.tr.tr_align <- 1  }
        
        | LABEL EQUAL s = QUOTED_STRING                    { !Dep2pict_global.tr.tr_label <- s }
        
        | FORECOLOR EQUAL v = VALUE                        { !Dep2pict_global.tr.tr_forecolor <- v }
        | FORECOLOR EQUAL v = QUOTED_STRING                { !Dep2pict_global.tr.tr_forecolor <- v }
        
        | COLOR EQUAL v = VALUE                            { !Dep2pict_global.tr.tr_color <- v }
        | COLOR EQUAL v = QUOTED_STRING                    { !Dep2pict_global.tr.tr_color <- v }
        
        | BGCOLOR EQUAL v = VALUE                          { !Dep2pict_global.tr.tr_bgcolor <- v }
        | BGCOLOR EQUAL v = QUOTED_STRING                  { !Dep2pict_global.tr.tr_bgcolor <- v }
        
        | STYLE EQUAL v = VALUE                            { !Dep2pict_global.tr.tr_style <- v }
        | STYLE EQUAL v = QUOTED_STRING                    { !Dep2pict_global.tr.tr_style <- v }
        
        | TOP                                              { !Dep2pict_global.tr.tr_top <- true }
        | BOTTOM                                           { !Dep2pict_global.tr.tr_top <- false || !Dep2pict_global.graph.gr_graph }
        
        | LABEL_OFFSET_POS EQUAL v = INT                   { !Dep2pict_global.tr.tr_label_offset_pos <- Dep2pict_utils.fos v }
        | LABEL_OFFSET_POS EQUAL v = QUOTED_STRING         { !Dep2pict_global.tr.tr_label_offset_pos <- Dep2pict_utils.fos v }
        
        | LABEL_OFFSET_NEG EQUAL v = INT                   { !Dep2pict_global.tr.tr_label_offset_neg <- Dep2pict_utils.fos v }
        | LABEL_OFFSET_NEG EQUAL v = QUOTED_STRING         { !Dep2pict_global.tr.tr_label_offset_neg <- Dep2pict_utils.fos v }
        
        | UNORIENTED                                       { !Dep2pict_global.tr.tr_oriented <- false; }
;

%%
