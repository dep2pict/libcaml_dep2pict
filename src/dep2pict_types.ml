type word = {
  input_id: string;
  mutable windex:int;
  (* svg attributes *)
  mutable wx:float;
  mutable wy:float;
  mutable wwidth:float;
  mutable wheight:float;
  mutable wnb_edges_left_top:int;
  mutable wnb_edges_right_top:int;
  mutable wnb_edges_left_bottom:int;
  mutable wnb_edges_right_bottom:int;
  mutable windex_x:int;
  (* dep attributes *)
  mutable text:string;
  mutable italic:bool;
  mutable bold:bool;
  mutable subword:string;
  mutable forecolor:string;
  mutable subcolor:string;
}

type group = {
  mutable gid:string;
  mutable gindex:int;
  (* svg attributes *)
  mutable gx:float;
  mutable gy:float;
  mutable gwidth:float;
  mutable gheight:float;
  mutable gnb_edges_left:int;
  mutable gnb_edges_right:int;
  mutable gindex_x:int;
  (* dep attributes *)
  mutable gnodes:int list;
  mutable gbordercolor:string;
  mutable gbackground:string;
  mutable gsubword:string;
  mutable gsupword:string;
}

type error = {
  mutable line:int;
  mutable error:string;
}

type node =
  | Word of word
  | Group of group


module Nodes = Set.Make (struct
  type t = node
  let compare a b =
    match a,b with
      | (Word w1, Word w2) -> compare w1.windex w2.windex
      | (Group g1, Word w2) -> compare g1.gindex w2.windex
      | (Word w1, Group g2) -> compare w1.windex g2.gindex
      | (Group g1, Group g2) -> compare g1.gindex g2.gindex
end)

(** Recuperation d'un mot a partir de son index **)
let get_word windex nodes =
  match Nodes.elements (Nodes.filter (function Word w1 -> w1.windex = windex | Group _ -> false) nodes) with
    | [one] -> one
    | _ -> raise Not_found

let cardinal_words nodes =
  Nodes.cardinal (Nodes.filter (function Word _ -> true | Group _ -> false) nodes)

(** Recuperation d'un groupe a partir de son index **)
let get_group gindex nodes =
  match Nodes.elements (Nodes.filter (function Group g1 -> g1.gindex = gindex | Word _ -> false) nodes) with
    | [one] -> one
    | _ -> raise Not_found

type words_edges = {
  mutable edge_id: string;
  (* dep attributes *)
  mutable wefrom_node:int;
  mutable weto_node:int;
  mutable wecolor:string;
  mutable weforecolor:string;
  mutable webgcolor:string;
  mutable welabel:string;
  mutable westyle:string;
  mutable welabel_offset_pos:float;
  mutable welabel_offset_neg:float;
  mutable welabel_align:int;
  mutable wetop:bool;
  mutable weoriented:bool;
  (* svg edge attributes *)
  mutable weindex:int;
  mutable welevel:float;
  mutable wex1:float;
  mutable wex2:float;
  mutable wex3:float;
  mutable wex4:float;
  mutable wey1:float;
  mutable wey2:float;
  mutable wey3:float;
  mutable wey4:float;
}

type groups_edges = {
  mutable gedge_id: string;
  (* dep attributes *)
  mutable gefrom_node:int;
  mutable geto_node:int;
  mutable gecolor:string;
  mutable geforecolor:string;
  mutable gebgcolor:string;
  mutable gelabel:string;
  mutable gestyle:string;
  mutable gelabel_offset_pos:float;
  mutable gelabel_offset_neg:float;
  mutable gelabel_align:int;
  mutable georiented:bool;
  (* svg edge attributes *)
  mutable geindex:int;
  mutable gelevel:float;
  mutable gex1:float;
  mutable gex2:float;
  mutable gex3:float;
  mutable gex4:float;
  mutable gey1:float;
  mutable gey2:float;
  mutable gey3:float;
  mutable gey4:float;
  mutable gefrom_word:bool;
}

type edge =
  | Word_edge of words_edges
  | Group_edge of groups_edges

module Edges = Set.Make (struct
  type t = edge
  let compare a b =
    match (a,b) with
      | (Word_edge _, Group_edge _) -> -1
      | (Group_edge _, Word_edge _) -> -1
      | (Word_edge w1, Word_edge w2) ->
	let x = abs (w1.wefrom_node-w1.weto_node)
	and y = abs (w2.wefrom_node-w2.weto_node) in
        if x !=y
        then compare x y
        else
          if w1.wefrom_node != w2.wefrom_node
          then compare w1.wefrom_node w2.wefrom_node
          else
            if w1.weto_node != w2.weto_node
            then compare w1.weto_node w2.weto_node
            else 1
      | Group_edge g1, Group_edge g2 ->
	let x = abs (g1.gefrom_node-g1.geto_node)
	and y = abs (g2.gefrom_node-g2.geto_node)
	in if (x!=y) then (
	  compare x y
	) else if (g1.gefrom_node != g2.gefrom_node) then (
	  compare g1.gefrom_node g2.gefrom_node
	) else if (g1.geto_node != g2.geto_node) then (
	  compare g1.geto_node g2.geto_node
	) else (
	  1
	)
end)

type graph = {
  mutable nodes:Nodes.t;
  mutable edges:Edges.t;

  mutable gr_graph:bool;
  mutable scale:int;

  mutable background:string;
  mutable opacity:float;
  mutable vspace:float;
  mutable hspace:float;
  mutable word_spacing:float;
  mutable subword_size:int;
  mutable subgroup_size:int;
  mutable supgroup_size:int;
  mutable edge_label_size:int;
  mutable margin_left:float;
  mutable margin_right:float;
  mutable margin_top:float;
  mutable margin_bottom:float;
  mutable fontname:string;
  mutable word_size:int;
}

let graph_reverse graph =
  let () = graph.nodes <-
    Nodes.map
      (function
        | Word w -> Word {w with windex = - w.windex}
        | Group g -> Group {g with gindex = - g.gindex}
      ) graph.nodes in
  let () = graph.edges <-
    Edges.map
      (function
        | Word_edge we ->
          Word_edge {we with wefrom_node = - we.wefrom_node; weto_node = - we.weto_node }
        | Group_edge ge -> Group_edge {ge with gefrom_node = - ge.gefrom_node; geto_node = - ge.geto_node }
      ) graph.edges in
  ()

let graph = ref {
  nodes=Nodes.empty;
  edges=Edges.empty;
  gr_graph=true;
  scale=100;
  background="white";
  opacity=1.;
  vspace=10.;
  hspace=5.;
  word_spacing=20.;
  subword_size=7;
  supgroup_size=7;
  edge_label_size=5;
  subgroup_size=7;
  margin_bottom=10.;
  margin_left=10.;
  margin_right=10.;
  margin_top=10.;
  word_size=12;
  fontname="Verdana";
}

module Edges2 = Set.Make (struct
  type t = edge

  let compare a b =
    match a,b with
      | Word_edge w1, Word_edge w2 ->
	if (abs (truncate (w1.wex1-.w1.wex4))) > (abs (truncate (w2.wex1-.w2.wex4))) then 1 else -1
      | Word_edge w1, Group_edge g2 ->
	if (abs (truncate (w1.wex1-.w1.wex4))) > (abs (truncate (g2.gex1-.g2.gex4))) then 1 else -1
      | Group_edge g1, Word_edge w2 ->
	if (abs (truncate (g1.gex1-.g1.gex4))) > (abs (truncate (w2.wex1-.w2.wex4))) then 1 else -1
      | Group_edge g1, Group_edge g2 ->

	let nodeA = try	get_word g1.gefrom_node !graph.nodes
	  with Not_found -> get_group g1.gefrom_node !graph.nodes in
	let nodeB = try	get_word g1.geto_node !graph.nodes
	  with Not_found -> get_group g1.geto_node !graph.nodes in
	let efrom = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
	let eto = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in

	let nodeA = try	get_word g2.gefrom_node !graph.nodes
	  with Not_found -> get_group g2.gefrom_node !graph.nodes in
	let nodeB = try	get_word g2.geto_node !graph.nodes
	  with Not_found ->  get_group g2.geto_node !graph.nodes in
	let e2from = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
	let e2to = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in
	if (abs (efrom-eto)) > (abs (e2from-e2to)) then 1 else -1
end)

module Edges3 = Set.Make (struct
  type t = edge

  let compare a b =
    match a,b with
      | Word_edge w1, Word_edge w2 ->
	if (abs (truncate (w1.wex1-.w1.wex4))) > (abs (truncate (w2.wex1-.w2.wex4))) then -1 else 1
      | Word_edge w1, Group_edge g2 ->
	if (abs (truncate (w1.wex1-.w1.wex4))) > (abs (truncate (g2.gex1-.g2.gex4))) then -1 else 1
      | Group_edge g1, Word_edge w2 ->
	if (abs (truncate (g1.gex1-.g1.gex4))) > (abs (truncate (w2.wex1-.w2.wex4))) then -1 else 1
      | Group_edge g1, Group_edge g2 ->

	let nodeA = try	get_word g1.gefrom_node !graph.nodes
	  with Not_found -> get_group g1.gefrom_node !graph.nodes in
	let nodeB = try	get_word g1.geto_node !graph.nodes
	  with Not_found -> get_group g1.geto_node !graph.nodes in
	let efrom = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
	let eto = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in

	let nodeA = try	get_word g2.gefrom_node !graph.nodes
	  with Not_found -> get_group g2.gefrom_node !graph.nodes in
	let nodeB = try	get_word g2.geto_node !graph.nodes
	  with Not_found ->  get_group g2.geto_node !graph.nodes in
	let e2from = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
	let e2to = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in
	if (abs (efrom-eto)) > (abs (e2from-e2to)) then -1 else 1
end)

let tranform_edges_to_edges2 g =
  let edges2 = ref Edges2.empty in
  Edges.iter ( fun e ->
    edges2 := Edges2.add e !edges2
  ) g.edges;
  graph := g;
  !edges2

let tranform_edges_to_edges3 g =
  let edges3 = ref Edges3.empty in
  Edges.iter ( fun e ->
    edges3 := Edges3.add e !edges3
  ) g.edges;
  graph := g;
  !edges3

module StringMap = Map.Make (String)

type wg = W | G

type transitionnal = {
  mutable tr_align:int;
  mutable tr_label:string;
  mutable tr_forecolor:string;
  mutable tr_color:string;
  mutable tr_bgcolor:string;
  mutable tr_style:string;
  mutable tr_top:bool;
  mutable tr_label_offset_pos:float;
  mutable tr_label_offset_neg:float;
  mutable tr_oriented:bool;
}
