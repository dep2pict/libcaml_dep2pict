module Dep2pictlib : sig
  val set_verbose : unit -> unit

  exception Error of Yojson.Basic.t
  type t

  val from_dep : ?rtl:bool -> string -> t
  val load_dep : ?rtl:bool -> string -> t

  val to_svg : t -> string
  val save_svg : filename:string -> t -> unit

  val save_pdf : filename:string -> t -> unit

  val save_png : filename:string -> t -> unit

  (** [highlight_shift ()] returns the leftmost position highlighted in the last dep struct built. *)
  val highlight_shift: unit -> float option
end
