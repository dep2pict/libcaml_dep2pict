
open Dep2pict_types

let (i:(int*wg) StringMap.t ref) = ref StringMap.empty
let index = ref 0

let debug = ref false

let is_for_words = ref true
let line = ref 1


let (tr:Dep2pict_types.transitionnal ref) = ref {
	tr_align=0;
	tr_label="";
	tr_forecolor="black";
	tr_color="black";
	tr_bgcolor="transparent";
	tr_style="solid";
	tr_top=true;
	tr_label_offset_pos=0.;
	tr_label_offset_neg=0.;
	tr_oriented=true;
}

let graph = ref { 
	nodes=Nodes.empty; 
	edges=Edges.empty;  
	gr_graph=true;
	scale=100;
	background="white"; 
	opacity=1.; 
	vspace=10.; 
	hspace=5.; 
	word_spacing=20.; 
	subword_size=7;
	subgroup_size=7;
	supgroup_size=7;
	edge_label_size=7;
	margin_bottom=10.; 
	margin_left=10.; 
	margin_right=10.; 
	margin_top=10.; 
	word_size=12;
	fontname="Verdana";
}

let word = ref {
  input_id="Unset";
	windex=(-1);
	wx=0.;
	wy=0.;
	wwidth=0.;
	wheight=0.;
	text="";
	italic=false;
	bold=false;
	subword="";
	forecolor="black";
	subcolor="black";
	wnb_edges_left_top=0;
	wnb_edges_right_top=0;
	wnb_edges_left_bottom=0;
	wnb_edges_right_bottom=0;
	windex_x=0;
}

let group = ref {
	gid="";
	gindex=(-1);
	gx=0.;
	gy=0.;
	gwidth=0.;
	gheight=0.;
	gnodes=[];
	gbordercolor="black";
	gnb_edges_left=0;
	gnb_edges_right=0;
	gindex_x=0;
	gbackground="white";
	gsubword="";
	gsupword="";
}

let wedge = ref {
  edge_id = "Unset";
	wefrom_node = (-1);
	weto_node = (-1);
	wecolor = "black";
	weforecolor = "black";
	webgcolor = "transparent";
	westyle="solid";
	weindex = 0;
	welevel = 0.;
	welabel = "";
	welabel_offset_pos=0.;
	welabel_offset_neg=0.;
	welabel_align=0;
	weoriented=true;
	wetop=true;
	wex1 = 0.;
	wex2 = 0.;
	wex3 = 0.;
	wex4 = 0.;
	wey1 = 0.;
	wey2 = 0.;
	wey3 = 0.;
	wey4 = 0.;
}

let gedge = ref {
  gedge_id = "Unset";
	gefrom_node = (-1);
	geto_node = (-1);
	gecolor = "black";
	geforecolor = "black";
	gebgcolor = "transparent";
	gestyle="solid";
	geindex = 0;
	gelevel = 0.;
	gelabel = "";
	gelabel_offset_pos=0.;
	gelabel_offset_neg=0.;
	gelabel_align=0;
	georiented=true;
	gex1 = 0.;
	gex2 = 0.;
	gex3 = 0.;
	gex4 = 0.;
	gey1 = 0.;
	gey2 = 0.;
	gey3 = 0.;
	gey4 = 0.;
	gefrom_word=false;
}

let reinit() = 
	i := StringMap.empty;
	line := 1;
	is_for_words := true;
	index := 0;
		tr := {
		tr_align=0;
		tr_label="";
		tr_forecolor="black";
		tr_color="black";
		tr_bgcolor="transparent";
		tr_style="solid";
		tr_top=true;
		tr_label_offset_pos=0.;
		tr_label_offset_neg=0.;
		tr_oriented=true;
	};
	graph := { 
		nodes=Nodes.empty; 
		edges=Edges.empty; 
		gr_graph=true;
		scale=100;
		background="white"; 
		opacity=1.; 
		vspace=10.; 
		hspace=5.; 
		subword_size=7;
		subgroup_size=7;
		supgroup_size=7;
		edge_label_size=7;
		word_spacing=20.; 
		margin_bottom=10.; 
		margin_left=10.; 
		margin_right=10.; 
		margin_top=10.; 
		word_size=12;
		fontname="Verdana";
	};
	word := {
  input_id="Unset";
		windex=(-1);
		wx=0.;
		wy=0.;
		wwidth=0.;
		wheight=0.;
		text="";
		italic=false;
		bold=false;
		subword="";
		forecolor="black";
		subcolor="black";
		wnb_edges_left_top=0;
		wnb_edges_right_top=0;
		wnb_edges_left_bottom=0;
		wnb_edges_right_bottom=0;
		windex_x=0;
	};
	group := {
		gid="";
		gindex=(-1);
		gx=0.;
		gy=0.;
		gwidth=0.;
		gheight=0.;
		gnodes=[];
		gbordercolor="black";
		gnb_edges_left=0;
		gnb_edges_right=0;
		gindex_x=0;
		gbackground="white";
		gsubword="";
		gsupword="";
	};
	wedge := {
          edge_id= "Unset";
		wefrom_node = (-1);
		weto_node = (-1);
		wecolor = "black";
		weforecolor = "black";
	        webgcolor = "transparent";
		westyle="solid";
		weindex = 0;
		welevel = 0.;
		welabel_offset_pos=0.;
		welabel_offset_neg=0.;
		welabel="";
		welabel_align=0;
		weoriented=true;
		wetop=true;
		wex1 = 0.;
		wex2 = 0.;
		wex3 = 0.;
		wex4 = 0.;
		wey1 = 0.;
		wey2 = 0.;
		wey3 = 0.;
		wey4 = 0.;
	};
	gedge := {
  gedge_id = "Unset";
		gefrom_node = (-1);
		geto_node = (-1);
		gecolor = "black";
		geforecolor = "black";
	        gebgcolor = "transparent";
		gestyle="solid";
		geindex = 0;
		gelabel_offset_pos=0.;
		gelabel_offset_neg=0.;
		gelabel_align=0;
		georiented=true;
		gelevel = 0.;
		gelabel = "";
		gex1 = 0.;
		gex2 = 0.;
		gex3 = 0.;
		gex4 = 0.;
		gey1 = 0.;
		gey2 = 0.;
		gey3 = 0.;
		gey4 = 0.;
		gefrom_word=false;
	}
	
	let tmp_string = ref ""
	let escaped = ref false

