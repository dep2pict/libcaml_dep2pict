open Printf

open Dep2pict_types
open Dep2pict_utils
open Font_utilities

let _ = at_exit (Font_utilities.remove_tmp_file)

module Dep2pictlib = struct

  let set_verbose () = verbose := true

  (** A global variable to record where is the leftmost highlighed part in the drawing (used in annot app) *)
  let leftmost_highlight = ref None
  let update_leftmost_highlight new_value =
    match !leftmost_highlight with
    | None -> leftmost_highlight := Some new_value
    | Some old_value when new_value < old_value -> leftmost_highlight := Some new_value
    | _ -> ()

  exception Error of Yojson.Basic.t

  let error ?file ?line ?fct ?data msg =
  let opt_list = [
    Some ("message", `String msg);
    (CCOption.map (fun x -> ("file", `String x)) file);
    (CCOption.map (fun x -> ("line", `Int x)) line);
    Some ("library", `String "libcaml_dep2pict");
    (CCOption.map (fun x -> ("function", `String x)) fct);
    (CCOption.map (fun x -> ("data", `String x)) data);
  ] in
  let json = `Assoc (CCList.filter_map (fun x->x) opt_list) in
  raise (Error json)


  let handle fct =
    try fct () with
      | Dep2pict_utils.Cant_find_index index -> error (sprintf "Unknown index '%s'" index)
      | Dep2pict_utils.Loop_in_dependencies msg -> error (sprintf "Loop in dependencies '%s'" msg)
      | Dep2pict_utils.Id_already_in_use id -> error (sprintf "Id already used '%s'" id)

  (* the code to add in svg item to react "on mouse over" when svg is produced with debug option *)
  let omo () =
    if !Dep2pict_global.debug
    then "onmouseover=\"evt.target.setAttribute('opacity', '0.5');\" onmouseout=\"evt.target.setAttribute('opacity','1)');\""
    else ""

  let title id =
    if !Dep2pict_global.debug
    then sprintf "<title>%s</title>" id
    else ""

  let pad = 2.

  let base_text_attribs = [
    ("text-anchor", "middle");
    ("text-align", "center")
  ] @
    if !Dep2pict_global.debug
    then [
      ("onmouseover", "evt.target.setAttribute('opacity', '0.5');");
      ("onmouseout", "evt.target.setAttribute('opacity','1)');");
    ]
    else []

  let replaces list text =
    List.fold_left
      (fun acc (regexp, replace) -> Str.global_replace (Str.regexp regexp) replace acc)
      text list

  let iteri fct =
    let rec loop i = function
      | [] -> ()
      | h::t -> (fct i h); (loop (i+1) t)
    in loop 0

  let get_base s =
    match Str.split (Str.regexp ":[A-Z]:") s with
      | [] -> ANSITerminal.eprintf [ANSITerminal.red] "FAIL: illegal subword >>>%s<<<\n" s; exit 1
      | text::_ -> text

  let get_att att s =
    let rec loop = function
      | (Str.Delim d) :: (Str.Text value) :: _ when d=att-> Some value
      | _::t -> loop t
      | _ -> None in
    loop (Str.full_split (Str.regexp ":[A-Z]:") s)

  (* ----------------------------------------------------------------------------- *)
  let first_level_top_to_add = ref 0.

  let protected_sharp_split string =
    let protected_string =
      replaces [
        ("##", "__SHARP__#");
        (":C:#", ":C:__SHARP__");
        (":B:#", ":B:__SHARP__");
      ] string in
    let protected_list = Str.split (Str.regexp "#") protected_string in
    List.map (replaces [("__SHARP__", "#")]) protected_list

  (* ----------------------------------------------------------------------------- *)
  let svg_escape text =
    Str.global_replace (Str.regexp "<") "&lt;"
      (Str.global_replace (Str.regexp ">") "&gt;"
         (Str.global_replace (Str.regexp "\"") "&quot;"
            (Str.global_replace (Str.regexp "&") "&amp;" text
            )
         )
      )

  (* ----------------------------------------------------------------------------- *)
  let nodes_placement graph =
    let x_orig = ref (0.+.graph.margin_left) in
    Nodes.iter
      (fun n ->
        match n with
          | Word word ->
            let windex = word.windex in
            let arity = Edges.fold
              (fun edge acc ->
                match edge with
                  | Word_edge we when we.wefrom_node = windex || we.weto_node = windex -> acc+1
                  | _ -> acc
              ) graph.edges 0 in

            let max_width = ref (graph.hspace *. (float arity)) in

            let lines = protected_sharp_split (svg_escape word.text) in
            let sublines = List.map get_base (protected_sharp_split (svg_escape word.subword)) in
            List.iter
              (fun s ->
                let text = get_base s in
                let (w,_,_) = Font_utilities.get_text_dimension graph.fontname graph.word_size text ~bold:word.bold ~italic:word.italic in
                max_width := max !max_width w;
              ) lines;
            List.iter
              (fun s ->
                let (w,_,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size s in
                (* printf "%s -> %f\n%!" s w; *)
                max_width := max !max_width w;
              ) sublines;
            let (_,h,d) = Font_utilities.get_text_dimension graph.fontname graph.word_size word.text ~bold:word.bold ~italic:word.italic in
            (* let (w2,h2,d2) = Font_utilities.get_text_dimension graph.fontname graph.subword_size word.subword in *)
            word.wwidth <- !max_width ;
            if List.length lines > 1
            then (
              word.wheight <- (float_of_int (List.length lines) *. (h+.2.));
              word.wy <- -1.*.d+.1. +. (float_of_int (List.length lines-1) *. (h+.2.));
            )
            else (
              word.wheight <- h;
              word.wy <- -1.*.d+.1.;
            );
            (* if (List.length sublines > 1) then ( *)
            (*   word.wheight <- word.wheight +. (float_of_int (List.length sublines) *. (h2+.2.)); *)
            (*   word.wy <- word.wy +. (float_of_int (List.length sublines) *. (h2+.2.)); *)
            (* ); *)
            word.wx <- !x_orig;
            x_orig := !x_orig +. graph.word_spacing +. word.wwidth;
          | Group _ -> ()
      ) graph.nodes

  (* ----------------------------------------------------------------------------- *)
  let get_dimension graph =
    let max_width = ref (0.+.graph.margin_left) in
    let max_height = ref 0. in
    let max_height2 = ref 0. in
    let trans_y = ref 0. in
    let trans_x = ref 0. in
    (*  let first_subword = ref true in*)
    let first_subword2 = ref true in
    Nodes.iter
      (fun n ->
        match n with
          | Word word ->
            max_width := !max_width +. graph.word_spacing +. word.wwidth;

            let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.word_size word.text in
            let splitted = protected_sharp_split (svg_escape word.text) in
            max_height := max !max_height (float_of_int (List.length splitted) *. (h+.2.));
            trans_y := max !trans_y h;

            let top = (float_of_int (List.length splitted) *. (h+.2.)) in

            if word.subword <> ""
            then
              begin
                let (_,h,d) = Font_utilities.get_text_dimension graph.fontname graph.subword_size word.subword in
                (* printf "%s\n%!" word.subword;*)
                let splitted = protected_sharp_split (svg_escape word.subword) in
                let hght = (float_of_int (List.length splitted) *. (h+.2.)) in
                max_height := max !max_height (top +. hght +. d);
              end
          | Group g ->
            max_width := max !max_width (g.gx+.g.gwidth+.20.);
            if g.gx < 0.
            then (
              max_width := !max_width +. abs_f(g.gx);
              trans_x := !trans_x +. abs_f(g.gx) +.1.;
            );
            if g.gsubword <> "" && !first_subword2
            then (
              first_subword2 := false;
              let (_,h,d) = Font_utilities.get_text_dimension graph.fontname graph.subgroup_size g.gsubword in
              max_height2 := max !max_height2 (h +. d +. 5.);
            );
            if g.gsubword = ""
            then (max_height2 := max !max_height2 (7.));
      ) graph.nodes;

    max_height := !max_height +. !max_height2;

    let max_level = ref (-1.) in
    let edge_label = ref "" in
    let y_max = ref 0. in
    Edges.iter
      (fun e ->
        match e with
          | Group_edge _ -> ()
          | Word_edge e ->
            if e.wetop
            then (
              let old = !max_level in
              max_level := max !max_level e.welevel;
              if old != !max_level
              then (
                edge_label := e.welabel;
                y_max := (-1.*.e.wey2) -. (-1.*.e.wey1);
              )
            )
      ) graph.edges;

    if !max_level > 0.
    then (
      let (_,h,d) = Font_utilities.get_text_dimension graph.fontname graph.edge_label_size !edge_label in
      max_height := !max_height +. (!y_max) +. h +. d +. 2.;
      trans_y := !trans_y +. !y_max +. h +.d +. 2.;
    );

    let max_level = ref (-1.) in
    let edge_label = ref "" in
    let y_max = ref 0. in
    Edges.iter
      (fun e ->
        match e with
          | Word_edge _ -> ()
          | Group_edge e ->
            let old = !max_level in
            max_level := max !max_level e.gelevel;
            if (old != !max_level) then (
              edge_label := e.gelabel;
              y_max := e.gey2-.(!max_height-. !trans_y);
            )
      ) graph.edges;
    if !max_level >= 0.
    then (
      let (_,h,d) = Font_utilities.get_text_dimension graph.fontname graph.edge_label_size !edge_label in
      let tmp = !max_height +. !y_max in
      max_height :=  tmp +. h +. d +. 10.;
    );

    max_width := !max_width -. graph.word_spacing +. graph.margin_right;

    max_height := !max_height +. !first_level_top_to_add;
    trans_y := !trans_y +. !first_level_top_to_add;

    (
      !max_height +. graph.margin_top +. graph.margin_bottom,
      !max_width+.2.,
      !trans_y +. graph.margin_top,
      !trans_x
    )

  (* ----------------------------------------------------------------------------- *)
  let nodes_to_svg graph =
    let buff = Buffer.create 32 in
    Nodes.iter
      (fun n -> match n with
        | Group g ->
          let local_nodes = Nodes.filter (fun n -> match n with Word n -> List.mem n.windex g.gnodes | _ -> false) graph.nodes in
          let max_height = ref 0. in
          let top = ref 0. in
          Nodes.iter
            (fun n -> match n with
              | Word word ->
                let (_,h2,_) = Font_utilities.get_text_dimension graph.fontname graph.word_size word.text in
                let splitted = protected_sharp_split (svg_escape word.text) in
                if (word.subword <> "")
                then (
                  let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size word.subword in
                  let splitted2 = protected_sharp_split (svg_escape word.subword) in
                  top := max !top ((float_of_int (List.length splitted2))*.(h+.2.)+.(float_of_int (List.length splitted-1))*.h2);
                  max_height := max !max_height ((float_of_int (List.length splitted))*.(h2+.2.)+.5.+.(float_of_int (List.length splitted2))*.(h+.2.));
                )
                else
                  (
                    max_height := max !max_height ((float_of_int (List.length splitted))*.(h2+.2.)+.5.);
                  )
              | _ -> failwith "Error"
            ) local_nodes;
          let first_node = match Nodes.min_elt local_nodes with Word w -> w | Group _ -> failwith "Error2" in
          let last_node = match Nodes.max_elt local_nodes with Word w -> w | Group _ -> failwith "Error3" in
          let (fst_pt_x,fst_pt_y) = (first_node.wx-.2.,first_node.wy+.2.+. !top+.5.) in
          let (snd_pt_x,snd_pt_y) = (first_node.wx-.2.,-1.*.(!max_height)-.2.+. !top+.5.) in
          let (thd_pt_x,thd_pt_y) = (last_node.wx+.last_node.wwidth+.5.,-1.*.(!max_height)-.2.+. !top+.5.) in
          let (fth_pt_x,fth_pt_y) = (last_node.wx+.last_node.wwidth+.5.,first_node.wy+.2.+. !top+.5.) in
          g.gx <- fst_pt_x;
          g.gy <- fst_pt_y +.4.;
          g.gwidth <- fth_pt_x -. fst_pt_x;
          if (g.gsubword<>"" || g.gsupword<>"")
          then (
            let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subgroup_size g.gsubword in
            let splitted = protected_sharp_split (svg_escape g.gsubword) in
            let h99 = float_of_int (List.length splitted) *. (h+.2.) in

            let (hsup,x1,y1,x2,y2) =
              if (g.gsupword<>"" && graph.supgroup_size<>0)
              then (
                let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.supgroup_size g.gsupword in
                let splitted = protected_sharp_split (svg_escape g.gsupword) in
                let h = float_of_int (List.length splitted) *. (h+.2.) in
                (h , fth_pt_x , snd_pt_y , fst_pt_x , snd_pt_y)
              )
              else (
                (0., fth_pt_x , fst_pt_y+.2. , fth_pt_x , fst_pt_y+.2.)
              ) in

            let y = snd_pt_y -. hsup in
            first_level_top_to_add := max !first_level_top_to_add hsup;

            if g.gsubword <> ""
            then (
              bprintf buff "<polyline stroke=\"%s\" fill=\"%s\" fill-opacity=\"1.0\" points=\"%s,%s,%s,%s,%s,%s,%s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s\"/>\n"
                g.gbordercolor g.gbackground
                (transform_float fst_pt_x) (transform_float (fst_pt_y+.h99+.3.)) (* * *)
                (transform_float snd_pt_x) (transform_float (y+.2.)) (* | *)
                (transform_float (snd_pt_x+.2.)) (transform_float y) (* / *)
                (transform_float (thd_pt_x-.2.)) (transform_float y) (* - *)
                (transform_float thd_pt_x) (transform_float (y+.2.)) (* \ *)
                (transform_float x1) (transform_float y1) (* | *)

                (transform_float x2) (transform_float y2) (* <- *)
                (transform_float x1) (transform_float y1) (* -> *)
                (transform_float fth_pt_x) (transform_float (fst_pt_y+.2.)) (* | *)

                (transform_float fst_pt_x) (transform_float (fst_pt_y+.2.)) (* <- *)
                (transform_float fth_pt_x) (transform_float (fst_pt_y+.2.)) (* -> *)

                (transform_float fth_pt_x) (transform_float (fth_pt_y+.h99+.3.)) (* | *)
                (transform_float (fth_pt_x-.2.)) (transform_float (fth_pt_y+.h99+.5.)) (* / *)
                (transform_float (fst_pt_x+.2.)) (transform_float (fst_pt_y+.h99+.5.)) (* - *)
                (transform_float fst_pt_x) (transform_float (fst_pt_y+.h99+.3.)); (* \ *)
            )
            else (
              bprintf buff "<polyline stroke=\"%s\" fill=\"%s\" fill-opacity=\"1.0\" points=\"%s,%s,%s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s\"/>\n"
                g.gbordercolor g.gbackground
                (transform_float fst_pt_x) (transform_float (fst_pt_y+.h99+.3.)) (* * *)
                (transform_float snd_pt_x) (transform_float (y+.2.)) (* | *)
                (transform_float (snd_pt_x+.2.)) (transform_float y) (* / *)
                (transform_float (thd_pt_x-.2.)) (transform_float y) (* - *)
                (transform_float thd_pt_x) (transform_float (y+.2.)) (* \ *)
                (transform_float x1) (transform_float y1) (* | *)

                (transform_float x2) (transform_float y2) (* <- *)
                (transform_float x1) (transform_float y1) (* -> *)
                (transform_float fth_pt_x) (transform_float (fst_pt_y+.2.)) (* | *)

                (transform_float fth_pt_x) (transform_float (fth_pt_y+.h99+.3.)) (* | *)
                (transform_float (fth_pt_x-.2.)) (transform_float (fth_pt_y+.h99+.5.)) (* / *)
                (transform_float (fst_pt_x+.2.)) (transform_float (fst_pt_y+.h99+.5.)) (* - *)
                (transform_float fst_pt_x) (transform_float (fst_pt_y+.h99+.3.)); (* \ *)
            (* * *)
            );
            if g.gsupword<>""
            then (
              let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.supgroup_size g.gsupword in
              (* BG: No identifier for supword text *)
              bprintf buff "<text x=\"%s\" y=\"%s\" style=\"fill:%s;font-size:%dpx;font-family:%s;text-anchor:middle;text-align:center\">\n%s</text>\n"
                (transform_float (g.gx+.g.gwidth/.2.))
                (transform_float (y+.h))
                "black"
                graph.supgroup_size
                graph.fontname
                (
                  let res = ref "" in
                  let splitted = protected_sharp_split (svg_escape g.gsupword) in
                  if (List.length splitted > 1)
                  then (
                    let i = ref 0 in
                    List.iter
                      (fun s ->
                        if !i = 0
                        then (res := "<tspan x=\""^(transform_float (g.gx+.g.gwidth/.2.))^"\">"^s^"</tspan>\n")
                        else (res := !res^"<tspan x=\""^(transform_float (g.gx+.g.gwidth/.2.))^"\" dy=\""^(transform_float (h+.2.))^"\">"^s^"</tspan>\n");
                        incr i;
                      ) splitted;
                  ) else
                    (res := (svg_escape g.gsupword));
                  !res
                );
            );

            (* BG: No identifier for this case *)
            bprintf buff "<text x=\"%s\" y=\"%s\" style=\"fill:%s;font-size:%dpx;font-family:%s;text-anchor:middle;text-align:center\">\n%s</text>\n"
              (transform_float (g.gx+.g.gwidth/.2.))
              (transform_float (g.gy+.h-.2.))
              "black"
              graph.subgroup_size
              graph.fontname
              (let res = ref "" in
               if (List.length splitted > 1)
               then (
                 let i = ref 0 in
                 List.iter
                   (fun s ->
                     if !i = 0
                     then (res := "<tspan  x=\""^(transform_float (g.gx+.g.gwidth/.2.))^"\">"^s^"</tspan>\n")
                     else (res := !res^"<tspan  x=\""^(transform_float (g.gx+.g.gwidth/.2.))^"\" dy=\""^(transform_float (h+.2.))^"\">"^s^"</tspan>\n");
                     incr i;
                   ) splitted;
               )
               else (res := (svg_escape g.gsubword));
               !res
              );
            g.gy <- fst_pt_y+.h99+.4.;
          )
          else (
            bprintf buff "<polyline stroke=\"%s\" fill=\"%s\" fill-opacity=\"1.0\" points=\"%s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s %s,%s\"/>\n"
              g.gbordercolor g.gbackground
              (transform_float fst_pt_x) (transform_float (fst_pt_y+.3.))
              (transform_float snd_pt_x) (transform_float (snd_pt_y+.2.))
              (transform_float (snd_pt_x+.2.)) (transform_float snd_pt_y)
              (transform_float (thd_pt_x-.2.)) (transform_float thd_pt_y)
              (transform_float thd_pt_x) (transform_float (thd_pt_y+.2.))
              (transform_float fth_pt_x) (transform_float (fth_pt_y+.3.))
              (transform_float (fth_pt_x-.2.)) (transform_float (fth_pt_y+.5.))
              (transform_float (fst_pt_x+.2.)) (transform_float (fst_pt_y+.5.))
              (transform_float fst_pt_x) (transform_float (fst_pt_y+.3.))
          )
        | _ -> ()
      ) graph.nodes;

    let span_line position_x delta_y line =
      let text = get_base line in
      match get_att ":C:" line with
        | None -> bprintf buff "<tspan   x=\"%g\" dy=\"%g\">%s</tspan>\n" position_x delta_y text
        | Some color -> bprintf buff "<tspan   x=\"%g\" dy=\"%g\" fill=\"%s\">%s</tspan>\n" position_x delta_y color text in

    Nodes.iter (function
      | Word w ->
        let lines = protected_sharp_split (svg_escape w.text) in
        let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.word_size w.text ~bold:w.bold ~italic:w.italic in
        let delta_y = h +. 2. in
        let position_x = w.wx +. w.wwidth /. 2. in
        let position_y = w.wy -. (float (List.length lines) *. delta_y) in

        (* draw background rectangles before text *)
        let _ = iteri
          (fun i line ->
            match get_att ":B:" line with
              | None -> ()
              | Some color ->
                let (w,_,_) = Font_utilities.get_text_dimension graph.fontname graph.word_size (get_base line) in
                update_leftmost_highlight position_x;
                bprintf buff "<rect x=\"%g\" y=\"%g\" width=\"%g\" height=\"%g\" style=\"fill:%s;fill-opacity:100;\" />\n"
                  (position_x -. w /. 2. -. pad)
                  (position_y +. (float i) *. delta_y +. pad)
                  (w +. 2. *. pad)
                  (delta_y +. 0. *. pad)
                  color
          ) lines in

        let text_attribs = [
          ("id", sprintf "word__%s" w.input_id);
          ("x", sprintf "%g" position_x);
          ("y", sprintf "%g" position_y);
          ("font-style", if w.italic then "italic" else "normal");
          ("font-weight", if w.bold then "bold" else "normal");
          ("fill", w.forecolor);
          ("font-size", string_of_int graph.word_size);
          ("font-family", graph.fontname);
        ] @ base_text_attribs in
        bprintf buff "<text %s>\n" (String.concat " " (List.map (fun (attrib,value) -> sprintf "%s=\"%s\"" attrib value) text_attribs));
        bprintf buff "%s" (title (sprintf "word__%s" w.input_id));
        List.iter (span_line position_x delta_y) lines;
        bprintf buff "</text>\n";

        if w.subword = ""
        then ()
        else
          begin
            let lines = protected_sharp_split (svg_escape w.subword) in
            let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size w.subword in
            let delta_y = h +. 2. in
            let position_x = w.wx +. w.wwidth /. 2. in
            let position_y = w.wy in

            (* draw background rectangles before text *)
            let _ = iteri
              (fun i line ->
                match get_att ":B:" line with
                  | None -> ()
                  | Some color ->
                    let (w,_,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size (get_base line) in
                    update_leftmost_highlight position_x;
                    bprintf buff "<rect x=\"%g\" y=\"%g\" width=\"%g\" height=\"%g\" style=\"fill:%s;fill-opacity:100;\" />\n"
                      (position_x -. w /. 2. -. pad)
                      (position_y +. (float i) *. delta_y +. pad)
                      (w +. 2. *. pad)
                      (delta_y +. 0. *. pad)
                      color
              ) lines in

            (* draw text *)
            let text_attribs = [
              ("id", sprintf "subword__%s" w.input_id);
              ("x", sprintf "%g" position_x);
              ("y", sprintf "%g" position_y);
              ("fill", w.forecolor);
              ("font-size", string_of_int graph.subword_size);
              ("font-family", graph.fontname);
            ] @ base_text_attribs in

            bprintf buff "<text %s>\n" (String.concat " " (List.map (fun (attrib,value) -> sprintf "%s=\"%s\"" attrib value) text_attribs));
            bprintf buff "%s" (title (sprintf "subword__%s" w.input_id));
            List.iter (span_line position_x delta_y) lines;
            bprintf buff "</text>\n";
          end
      | _ -> ()
    ) graph.nodes;
    Buffer.contents buff

  (* ----------------------------------------------------------------------------- *)
  let recalculate_index graph =
    Nodes.iter
      (fun n -> match n with
        | Word w -> w.windex_x <- truncate (w.wx +. (w.wwidth/.2.))
        | Group g -> g.gindex_x <- truncate (g.gx +. (g.gwidth/.2.))
      ) graph.nodes

  (* ----------------------------------------------------------------------------- *)
  let words_edges_to_svg graph =
    recalculate_index graph;
    let buff = Buffer.create 32 in
    Edges.iter
      (fun e -> match e with
        | Word_edge e ->
          (* calcul du nombre d'arcs sur la gauche et la droite des noeuds *)
          let nodeA = Dep2pict_types.get_word e.wefrom_node graph.nodes in
          let nodeB = Dep2pict_types.get_word e.weto_node graph.nodes in
          if (e.wefrom_node < e.weto_node)
          then (
            begin match nodeA with
              | Word w -> w.wnb_edges_right_top <- w.wnb_edges_right_top + 1;
              | _ -> ()
            end;
            begin match nodeB with
              | Word w -> w.wnb_edges_left_top <- w.wnb_edges_left_top + 1;
              | _ -> ()
            end;
          )
          else (
            begin match nodeA with
              | Word w -> w.wnb_edges_left_top <- w.wnb_edges_left_top + 1;
              | _ -> ()
            end;
            begin match nodeB with
              | Word w -> w.wnb_edges_right_top <- w.wnb_edges_right_top + 1;
              | _ -> ()
            end;
          );

          (* calcul du nombre d'arc en dessous du courant *)
          Edges.iter
            (fun e2 -> match e2 with
              | Word_edge e2 ->
                if (e2.weindex != e.weindex)
                then (
                  if (e.wefrom_node < e.weto_node)
                  then ( (* premier arc -> *)
                    if (e2.wefrom_node < e2.weto_node)
                    then ( (* deuxieme arc -> *)
                      let _1 = e.wefrom_node
                      and _2 = e2.wefrom_node
                      and _3 = e2.weto_node
                      and _4 = e.weto_node in
                      if (_1<=_2 && _3<=_4) then e.welevel <- max e.welevel (e2.welevel+.1.)
                    )
                    else
                      if (e2.wefrom_node > e2.weto_node) then ( (* deuxieme arc <- *)
                        let _1 = e.wefrom_node
                        and _2 = e2.weto_node
                        and _3 = e2.wefrom_node
                        and _4 = e.weto_node in
                        if (_1<=_2 && _3<=_4) then e.welevel <- max e.welevel (e2.welevel+.1.)
                      )
                  )
                  else
                    if (e.wefrom_node > e.weto_node)
                    then ( (* premier arc <- *)
                      if (e2.wefrom_node < e2.weto_node)
                      then ( (* deuxieme arc -> *)
                        let _1 = e.weto_node
                        and _2 = e2.wefrom_node
                        and _3 = e2.weto_node
                        and _4 = e.wefrom_node in
                        if (_1<=_2 && _3<=_4) then e.welevel <- max e.welevel (e2.welevel+.1.)
                      )
                      else if (e2.wefrom_node > e2.weto_node)
                      then ( (* deuxieme arc <- *)
                        let _1 = e.weto_node
                        and _2 = e2.weto_node
                        and _3 = e2.wefrom_node
                        and _4 = e.wefrom_node in
                        if (_1<=_2 && _3<=_4) then e.welevel <- max e.welevel (e2.welevel+.1.))
                    );
                  if (e.welevel < 1.) then (e.welevel <- 1.);
                );
              | _ -> ()
            )
            (Edges.filter (fun e -> match e with Word_edge e -> e.wetop | _ -> false) graph.edges);
        (* printf "from : %d; to : %d; level : %d<br/>%!" e.wefrom_node e.weto_node e.welevel;*)

        | Group_edge _ -> ()
      ) (Edges.filter (fun e -> match e with Word_edge e -> e.wetop | _ -> false) graph.edges);

    let compare_edges a b = match a,b with
      | Word_edge w1, Word_edge w2 ->
        let x = abs (w1.wefrom_node-w1.weto_node)
        and y = abs (w2.wefrom_node-w2.weto_node) in
        if (x!=y)
        then (compare x y)
        else
          if (w1.wefrom_node != w2.wefrom_node)
          then (compare w1.wefrom_node w2.wefrom_node)
          else
            if (w1.weto_node != w2.weto_node)
            then (compare w1.weto_node w2.weto_node)
            else 1
      | Word_edge _, Group_edge _ -> 0
      | Group_edge _, Word_edge _ -> 0
      | Group_edge _, Group_edge _ -> 0 in

    Edges.iter
      (fun e -> match e with
        | Word_edge e ->
          let level = ref 0 in
          Edges.iter
            (fun e2 -> match e2 with
              | Word_edge e2 ->
                if (e2.weindex != e.weindex)
                then (
                  (* calcul des niveaux pour les arcs croisés *)
                  let a = e2.wefrom_node
                  and b = e2.weto_node in
                  let c = e.wefrom_node
                  and d = e.weto_node in
                  if (c < d)
                  then ( (* premier arc -> *)
                    if (a > c && a < d && (b > d || b < c) && (truncate (e.welevel*.10.)) = (truncate (e2.welevel*.10.)))
                    then (e.welevel <- e2.welevel +.1.; incr level)
                    else
                      if (b > c && b < d && (a > d || a < c) && (truncate (e.welevel*.10.)) = (truncate (e2.welevel*.10.)))
                      then (e.welevel <- e2.welevel +.1.; incr level)
                  )
                  else ( (* premier arc <- *)
                    if (a < c && a > d && (b < d || b > c) && (truncate (e.welevel*.10.)) = (truncate (e2.welevel*.10.)))
                    then (e.welevel <- e2.welevel +.1.; incr level)
                    else
                      if (b < c && b > d && (a < d || a > c) && (truncate (e.welevel*.10.)) = (truncate (e2.welevel*.10.)))
                      then (e.welevel <- e2.welevel +.1.; incr level)
                  );
                );
              | _ -> ()
            )
            (Edges.filter (fun ee -> match ee with Word_edge eee -> eee.wetop && (compare_edges (Word_edge eee) (Word_edge e) = -1) | _ -> false) graph.edges);

          (*                                      printf "from : %d; to : %d; level : %f\n%!" e.wefrom_node e.weto_node e.welevel;*)
          if (!level > 0)
          then (
            let to_re_evaluate () =
              Edges.filter
                (fun ex -> match ex with
                  | Word_edge e3 ->
                    (* l'arc est en dessous de l'arc testé *)
                    if (e3.wetop && e3.weindex != e.weindex && e3.welevel >= e.welevel)
                    then (
                      (* printf "to re evaluate? : from : %d; to : %d; level : %f\n%!" e3.wefrom_node e3.weto_node e3.welevel;*)
                      if (abs(e3.wefrom_node-e3.weto_node) >= abs(e.wefrom_node-e.weto_node))
                      then (
                        let first = min e3.wefrom_node e3.weto_node
                        and second = min e.wefrom_node e.weto_node
                        and third = max e.wefrom_node e.weto_node
                        and fourth = max e3.wefrom_node e3.weto_node in

                        (* printf "/⁻\/-\  -> %d %d %d %d\n%!" first second third fourth ;*)
                        let one = first <= second && second <= third && third <= fourth in
                        let two = second <= first && first <= third && third <= fourth in
                        one || two && (truncate e.welevel) <= (truncate e3.welevel)
                      )
                      else ( (* l'arc est au dessus de l'arc testé *)
                        let first = min e3.wefrom_node e3.weto_node
                        and second = min e.wefrom_node e.weto_node
                        and third = max e3.wefrom_node e3.weto_node
                        and fourth = max e.wefrom_node e.weto_node in
                        let one = first <= second && second <= third && third <= fourth in
                        let two = second <= first && first <= fourth && fourth <= third in
                        one || two && (truncate e.welevel) <= (truncate e3.welevel)
                      )
                    )
                    else false
                  | _ -> false
                ) graph.edges in
            Edges.iter
              (fun e4 -> match e4 with
                | Word_edge e4 -> e4.welevel <- e4.welevel +. 1.
                | _ ->()
              ) (to_re_evaluate ());
          );
        (* e.welabel <- sprintf "%f" e.welevel;*)
        | Group_edge _ -> ()
      )
      (Edges.filter (fun e -> match e with Word_edge e -> e.wetop | _ -> false) graph.edges);

    Edges.iter
      (fun e -> match e with
        | Word_edge e ->
          if e.welevel < 1.
          then (e.welevel <- 1.);
        (* printf "from : %d; to : %d; level : %d<br/>" e.wefrom_node e.weto_node e.welevel*)
        | _ -> ()
      )
      (Edges.filter (fun e -> match e with Word_edge e -> e.wetop | _ -> false) graph.edges);

    (* decalage des arcs sur les noeuds *)
    Edges.iter
      (fun e -> match e with
        | Word_edge e ->
          let nodeA = Dep2pict_types.get_word e.wefrom_node graph.nodes in
          let nodeB = Dep2pict_types.get_word e.weto_node graph.nodes in

          (* printf "[DEBUG] from : %d; to : %d; level : %f\n%!" e.wefrom_node e.weto_node e.welevel;*)

          (* si arc positif -> *)
          if (e.wefrom_node < e.weto_node)
          then (
            (* decalage à droite pour le noeud d'origine *)
            let tmp = Edges.filter (fun ee -> match ee with
              | Word_edge ee -> ee.welevel > e.welevel && ((ee.wefrom_node = e.wefrom_node && ee.weto_node > e.wefrom_node) || (ee.weto_node = e.wefrom_node && ee.wefrom_node > e.wefrom_node))
              | _ -> false
            ) graph.edges in
            let tmp = float_of_int (Edges.cardinal tmp) in
            (* decalage à gauche pour le noeud d'arrivée *)
            let tmp2 = Edges.filter (fun ee -> match ee with
              | Word_edge ee -> ee.welevel > e.welevel && ((ee.wefrom_node = e.weto_node && ee.weto_node < e.weto_node) || (ee.weto_node = e.weto_node && ee.wefrom_node < e.weto_node))
              | _ -> false
            ) graph.edges in
            let tmp2 = float_of_int (Edges.cardinal tmp2 +1) in
            begin match nodeA,nodeB with
              | Word w1, Word w2 ->
                e.wex1 <- w1.wx+.(w1.wwidth/.2.)+.graph.hspace*.tmp;
                e.wex2 <- w1.wx+.(w1.wwidth/.2.)+.graph.hspace*.tmp;
                e.wex3 <- w2.wx+.(w2.wwidth/.2.)-.graph.hspace*.tmp2;
                e.wex4 <- w2.wx+.(w2.wwidth/.2.)-.graph.hspace*.tmp2;
                e.wey1 <- w1.wy-.w1.wheight-.1. -. !first_level_top_to_add;
                e.wey2 <- w1.wy-.w1.wheight-.graph.vspace*.e.welevel -. !first_level_top_to_add;
                e.wey3 <- w1.wy-.w1.wheight-.graph.vspace*.e.welevel -. !first_level_top_to_add;
                e.wey4 <- w2.wy-.w2.wheight-.1. -. !first_level_top_to_add;
              | _,_ -> ()
            end;
          )

          else ( (* arc <- *)
            (* decalage à gauche pour le noeud d'origine *)
            let tmp = Edges.filter (fun ee -> match ee with
              | Word_edge ee -> ee.welevel > e.welevel && ((e.wefrom_node = ee.wefrom_node && ee.weto_node < e.wefrom_node) || (ee.wefrom_node < e.wefrom_node && e.wefrom_node = ee.weto_node))
              | _ -> false
            ) graph.edges in
            let tmp = float_of_int (Edges.cardinal tmp +1) in
            (* decalage à droite pour le noeud d'arrivée *)
            let tmp2 = Edges.filter (fun ee -> match ee with
              | Word_edge ee -> ee.welevel > e.welevel && ((ee.wefrom_node = e.weto_node && ee.weto_node > e.weto_node) || (ee.weto_node = e.weto_node && ee.wefrom_node > e.weto_node) )
              | _ -> false
            ) graph.edges in
            let tmp2 = float_of_int (Edges.cardinal tmp2) in
            begin match nodeA,nodeB with
              | Word w1, Word w2 ->
                e.wex1 <- w1.wx+.(w1.wwidth/.2.)-.graph.hspace*.tmp;
                e.wex2 <- w1.wx+.(w1.wwidth/.2.)-.graph.hspace*.tmp;
                e.wex3 <- w2.wx+.(w2.wwidth/.2.)+.graph.hspace*.tmp2;
                e.wex4 <- w2.wx+.(w2.wwidth/.2.)+.graph.hspace*.tmp2;
                e.wey1 <- w1.wy-.w1.wheight-.1. -. !first_level_top_to_add;
                e.wey2 <- w1.wy-.w1.wheight-.graph.vspace*.e.welevel -. !first_level_top_to_add;
                e.wey3 <- w1.wy-.w1.wheight-.graph.vspace*.e.welevel -. !first_level_top_to_add;
                e.wey4 <- w2.wy-.w2.wheight-.1. -. !first_level_top_to_add;
              | _,_ -> ()
            end;
          );
        | Group_edge _ -> ()
      )
      (Edges.filter (fun e -> match e with Word_edge e -> e.wetop | _ -> false) graph.edges);

    (* recentrage des arcs sur les mots *)
    Edges.iter
      (fun e -> match e with
        | Word_edge e ->
          let nodeA = Dep2pict_types.get_word e.wefrom_node graph.nodes in
          let nodeB = Dep2pict_types.get_word e.weto_node graph.nodes in
          begin match nodeA,nodeB with
            | Word w1, Word w2 ->
              (* let c1 = w1.wx+.w1.wwidth/.2. in*)
              let diff1 = w1.wnb_edges_right_top - w1.wnb_edges_left_top - 1 in
              if (diff1 > 0)
              then (
                let dec = (foi (w1.wnb_edges_right_top - w1.wnb_edges_left_top)) /. 2. -. 0.5 in
                e.wex1 <- e.wex1 -. (dec*.graph.hspace);
                e.wex2 <- e.wex2 -. (dec*.graph.hspace);
              )
              else
                if (diff1 <= -1)
                then (
                  let dec = (foi (w1.wnb_edges_left_top - w1.wnb_edges_right_top)) /. 2. +. 0.5 in
                  e.wex1 <- e.wex1 +. (dec*.graph.hspace);
                  e.wex2 <- e.wex2 +. (dec*.graph.hspace);
                );
              (* let c2 = w2.wx+.w2.wwidth/.2. in*)
              let diff2 = w2.wnb_edges_right_top - w2.wnb_edges_left_top - 1 in
              if (diff2 > 0)
              then (
                let dec = (foi (w2.wnb_edges_right_top - w2.wnb_edges_left_top)) /. 2. -. 0.5 in
                e.wex3 <- e.wex3 -. (dec*.graph.hspace);
                e.wex4 <- e.wex4 -. (dec*.graph.hspace);
              )
              else
                if (diff2 <= -1)
                then (
                  let dec = (foi (w2.wnb_edges_left_top - w2.wnb_edges_right_top)) /. 2. +. 0.5 in
                  e.wex3 <- e.wex3 +. (dec*.graph.hspace);
                  e.wex4 <- e.wex4 +. (dec*.graph.hspace);
                );
            | _,_ -> ()
          end;

          let sign = if (e.wefrom_node < e.weto_node) then 1. else -.1. in
          bprintf buff "<marker id=\"markerendword_%s\" markerWidth=\"5\" markerHeight=\"5\" markerUnits=\"userSpaceOnUse\" orient=\"auto\" refX=\"2\" refY=\"2\">\n" (soi e.weindex);
          bprintf buff "<path d=\"M0,0 l4,2 l-4,2 z\" style=\"fill:%s;\"/>\n" e.wecolor;
          bprintf buff "</marker>\n";
          let label_id = sprintf "label__%s" e.edge_id in
          let path_id = sprintf "path__%s" e.edge_id in
          if (e.welabel<>"")
          then
            begin
              let (width,ascent,descent) = Font_utilities.get_text_dimension graph.fontname graph.edge_label_size e.welabel in
              let x = match e.welabel_align with
                | 0 -> float_of_int (abs(truncate(e.wex3-.e.wex2)))/.2.+.(min e.wex3 e.wex2)+.e.welabel_offset_pos-.e.welabel_offset_neg
                | 1 -> e.wex3
                | 2 -> e.wex1
                | _ -> float_of_int (abs(truncate(e.wex3-.e.wex2)))/.2.+.(min e.wex3 e.wex2)+.e.welabel_offset_pos-.e.welabel_offset_neg in
              let y = e.wey2 -. 2. in

              (* set text background color if needed *)
              (match e.webgcolor with
                | "transparent" -> ()
                | color ->
                  update_leftmost_highlight x;
                  bprintf buff "<rect x=\"%s\" y=\"%s\" width=\"%s\" height=\"%s\" style=\"fill:%s;fill-opacity:100;\" />\n"
                    (transform_float (x-.width/.2. -. pad))
                    (transform_float (y-.ascent -. pad))
                    (transform_float (width +. 2. *. pad))
                    (transform_float (ascent+.descent +. 2. *. pad))
                    color
              );

              bprintf buff "<text id=\"%s\" x=\"%s\" y=\"%s\" style=\"fill:%s;font-size:%dpx;font-family:%s;text-anchor:%s;text-align:%s\" %s>%s%s</text>\n"
                label_id (transform_float x) (transform_float y)
                e.weforecolor
                graph.edge_label_size
                graph.fontname
                (match e.welabel_align with 1 -> "end" | 2 -> "start" | _ -> "middle")
                (match e.welabel_align with 1 -> "right" | 2 -> "left" | _ -> "center")
                (omo ())
                (title label_id)
                (svg_escape e.welabel)
            end;
          bprintf buff "<path style=\"%s%s\" stroke=\"%s\" fill=\"none\" d=\"M %s,%s %s,%s A 3 3 0 0 %s %s,%s L %s,%s A 3 3 0 0 %s %s,%s L %s,%s\" %s>%s</path>\n"
            (if e.weoriented then sprintf "marker-end:url(#markerendword_%i);" e.weindex else "")
            (match e.westyle with
              | "dot" -> "stroke-dasharray:1,1;"
              | "dash" -> "stroke-dasharray:3,1;"
              | _ -> "")
            e.wecolor
            (transform_float e.wex1) (transform_float e.wey1)
            (transform_float e.wex2) (transform_float (e.wey2+.4.))
            (if e.wex3 > e.wex2 then "1" else "0")
            (transform_float (e.wex2+.(3.*.sign))) (transform_float (e.wey2+.1.))
            (transform_float (e.wex3-.(3.*.sign))) (transform_float (e.wey3+.1.))
            (if e.wex3 > e.wex2 then "1" else "0")
            (transform_float e.wex3) (transform_float (e.wey3+.4.))
            (transform_float e.wex4) (transform_float e.wey4)
            (omo ())
            (title path_id)
        | Group_edge _ ->  ()
      ) (Edges.filter (fun e -> match e with Word_edge e -> e.wetop | _ -> false) graph.edges);
    Buffer.contents buff

  (* ----------------------------------------------------------------------------- *)
  let words_edges_to_svg_bottom graph =
    if (not graph.gr_graph)
    then Edges.iter
      (function
        | Word_edge e ->
          graph.edges <- Edges.add
            (Group_edge
               { gedge_id = e.edge_id;
                 gefrom_node=e.wefrom_node;
                 geto_node=e.weto_node;
                 gecolor=e.wecolor;
                 geforecolor=e.weforecolor;
                 gebgcolor=e.webgcolor;
                 gelabel=e.welabel;
                 gestyle=e.westyle;
                 gelabel_offset_pos=e.welabel_offset_pos;
                 gelabel_offset_neg=e.welabel_offset_neg;
                 gelabel_align=e.welabel_align;
                 geindex=e.weindex;
                 georiented=e.weoriented;
                 gelevel=0.;
                 gex1=e.wex1;
                 gex2=e.wex2;
                 gex3=e.wex3;
                 gex4=e.wex4;
                 gey1=e.wey1;
                 gey2=e.wey2;
                 gey3=e.wey3;
                 gey4=e.wey4;
                 gefrom_word=true;
               }
            ) graph.edges
        | _ -> ()
      ) (Edges.filter (fun e -> match e with Word_edge e -> not e.wetop | _ -> false) graph.edges)

  (* ----------------------------------------------------------------------------- *)
  let groups_edges_to_svg graph =
    recalculate_index graph;
    let buff = Buffer.create 32 in
    Dep2pict_types.graph := graph;
    let edges2 = Dep2pict_types.tranform_edges_to_edges2 graph in
    let first = ref true in
    Edges2.iter
      (function
        | Word_edge _ -> ()
        | Group_edge e ->
          (* calcul du nombre d'arcs sur la gauche et la droite des noeuds *)
          let nodeA =
            try Dep2pict_types.get_word e.gefrom_node graph.nodes
            with Not_found -> Dep2pict_types.get_group e.gefrom_node graph.nodes in
          let nodeB =
            try Dep2pict_types.get_word e.geto_node graph.nodes
            with Not_found -> Dep2pict_types.get_group e.geto_node graph.nodes in
          begin
            match (nodeA, nodeB) with
              | (Word w, Group g)
              | (Group g, Word w) ->
                if w.windex_x < g.gindex_x
                then ( (* le mot est avant le groupe; +1 droite pour le mot; +1 gauche pour le groupe *)
                  w.wnb_edges_right_bottom <- w.wnb_edges_right_bottom + 1;
                  g.gnb_edges_left <- g.gnb_edges_left + 1;
                )
                else ( (* le mot est apres le groupe; +1 gauche pour le mot; +1 droite pour le groupe *)
                  w.wnb_edges_left_bottom <- w.wnb_edges_left_bottom + 1;
                  g.gnb_edges_right <- g.gnb_edges_right + 1;
                )
              | (Group g1, Group g2) ->
                if g1.gindex_x < g2.gindex_x
                then ( (* g1 est avant g2; +1 droite pour g1; +1 gauche pour g2 *)
                  g1.gnb_edges_right <- g1.gnb_edges_right + 1;
                  g2.gnb_edges_left <- g2.gnb_edges_left + 1;
                )
                else ( (* g1 est apres g2; +1 gauche pour g1; +1 droite pour g2 *)
                  g1.gnb_edges_left <- g1.gnb_edges_left + 1;
                  g2.gnb_edges_right <- g2.gnb_edges_right + 1;
                )
              | Word w1, Word w2 ->
                if w1.windex_x < w2.windex_x
                then ( (* le mot est avant le groupe; +1 droite pour le mot; +1 gauche pour le groupe *)
                  w1.wnb_edges_right_bottom <- w1.wnb_edges_right_bottom + 1;
                  w2.wnb_edges_left_bottom <- w2.wnb_edges_left_bottom + 1;
                )
                else ( (* le mot est apres le groupe; +1 gauche pour le mot; +1 droite pour le groupe *)
                  w1.wnb_edges_left_bottom <- w1.wnb_edges_left_bottom + 1;
                  w2.wnb_edges_right_bottom <- w2.wnb_edges_right_bottom + 1;
                )
          end;

          (* calcul du nombre d'arc en dessous du courant *)
          let efrom = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
          let eto = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in

          if !first
          then (
            e.gelevel <- 0. ;
            first := false;
          )
          else (
            Edges2.iter
              (fun e2 -> match e2 with
                | Group_edge e2 ->
                  let nodeA =
                    try Dep2pict_types.get_word e2.gefrom_node graph.nodes
                    with Not_found -> Dep2pict_types.get_group e2.gefrom_node graph.nodes in
                  let nodeB =
                    try Dep2pict_types.get_word e2.geto_node graph.nodes
                    with Not_found ->  Dep2pict_types.get_group e2.geto_node graph.nodes in
                  let e2from = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
                  let e2to = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in
                  if (e2.geindex != e.geindex)
                  then (
                    if (efrom < eto)
                    then ( (* premier arc -> *)
                      if (e2from < e2to)
                      then ( (* deuxieme arc -> *)
                        let _1 = efrom
                        and _2 = e2from
                        and _3 = e2to
                        and _4 = eto in
                        if (_1<=_2 && _3<=_4)
                        then (e.gelevel <- max e.gelevel (e2.gelevel+.1.);
                        (* e.gelabel <- "ICI1"; *)
                        (* printf "ICI1 : %d %d %d %d\n%!" _1 _2 _3 _4;*)
                        )
                      )
                      else
                        if (e2from > e2to)
                        then ( (* deuxieme arc <- *)
                          (* printf "ARG!!\n%!";*)
                          let _1 = efrom
                          and _2 = e2to
                          and _3 = e2from
                          and _4 = eto in
                          if (_1<=_2 && _3<=_4)
                          then (e.gelevel <- max e.gelevel (e2.gelevel+.1.);)
                        )
                    )
                    else
                      if (efrom >= eto)
                      then ( (* premier arc <- *)
                        if (e2from < e2to)
                        then ( (* deuxieme arc -> *)
                          let _1 = eto
                          and _2 = e2from
                          and _3 = e2to
                          and _4 = efrom in
                          if (_1<=_2 && _3<=_4) then (e.gelevel <- max e.gelevel (e2.gelevel+.1.))
                        )
                        else
                          if (e2from > e2to)
                          then ( (* deuxieme arc <- *)
                            let _1 = eto
                            and _2 = e2to
                            and _3 = e2from
                            and _4 = efrom in
                            if (_1<=_2 && _3<=_4)
                            then (e.gelevel <- max e.gelevel (e2.gelevel+.1.)
                            )
                          )
                      );
                    if (e.gelevel < 1.)
                    then (e.gelevel <- 1.)
                  );
                | _ -> ()
              ) edges2;
          );
      (* printf "%d->%d : %f\n%!" e.gefrom_node e.geto_node e.gelevel *)
      ) edges2;

    Edges2.iter
      (fun e -> match e with
        | Group_edge e ->
          let nodeA =
            try Dep2pict_types.get_word e.gefrom_node graph.nodes
            with Not_found -> Dep2pict_types.get_group e.gefrom_node graph.nodes in
          let nodeB =
            try Dep2pict_types.get_word e.geto_node graph.nodes
            with Not_found -> Dep2pict_types.get_group e.geto_node graph.nodes in
          let c = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
          let d = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in
          let level = ref 0 in
          Edges2.iter
            (fun e2 -> match e2 with
              | Group_edge e2 ->
                if (e2.geindex != e.geindex)
                then (
                  (* calcul des niveaux pour les arcs croisés *)
                  let nodeA =
                    try Dep2pict_types.get_word e2.gefrom_node graph.nodes
                    with Not_found -> Dep2pict_types.get_group e2.gefrom_node graph.nodes in
                  let nodeB =
                    try Dep2pict_types.get_word e2.geto_node graph.nodes
                    with Not_found ->  Dep2pict_types.get_group e2.geto_node graph.nodes in
                  let a = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
                  let b = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in
                  if (c < d)
                  then ( (* premier arc -> *)
                    if (a > c && a < d && (b > d || b < c) && (truncate (e.gelevel*.10.)) = (truncate (e2.gelevel*.10.)))
                    then (
                      e.gelevel <- e2.gelevel +.1.;
                      incr level;
                    )
                    else
                      if (b > c && b < d && (a > d || a < c) && (truncate (e.gelevel*.10.)) = (truncate (e2.gelevel*.10.)))
                      then (
                        e.gelevel <- e2.gelevel +.1.;
                        incr level;
                      )
                  )
                  else ( (* premier arc <- *)
                    if (a < c && a > d && (b < d || b > c) && (truncate (e.gelevel*.10.)) = (truncate (e2.gelevel*.10.)))
                    then (
                      e.gelevel <- e2.gelevel +.1.;
                      incr level;
                    ) else
                      if (b < c && b > d && (a < d || a > c) && (truncate (e.gelevel*.10.)) = (truncate (e2.gelevel*.10.)))
                      then (
                        e.gelevel <- e2.gelevel +.1.;
                        incr level;
                      )
                  );
                )
              | _ -> ()
            ) edges2;

          if (!level > 0)
          then (
            let to_re_evaluate = Edges.filter
              (fun ex -> match ex with
                | Group_edge e3 ->
                  if (e3.geindex != e.geindex && e3.gelevel >= e.gelevel) then (
                    (*                                                                          printf "to re evaluate? : from : %d; to : %d; level : %f\n%!" e3.gefrom_node e3.geto_node e3.gelevel;*)
                    let nodeA =
                      try Dep2pict_types.get_word e3.gefrom_node graph.nodes
                      with Not_found -> Dep2pict_types.get_group e3.gefrom_node graph.nodes in
                    let nodeB =
                      try Dep2pict_types.get_word e3.geto_node graph.nodes
                      with Not_found ->  Dep2pict_types.get_group e3.geto_node graph.nodes in
                    let a = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
                    let b = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in
                    if (abs(a-b) >= abs(c-d))
                    then (
                      let first = min a b and
                          second = min c d and
                          third = max c d and
                          fourth = max a b in
                      (* printf "/⁻\/-\  -> %d %d %d %d\n%!" first second third fourth ;*)
                      let one = first <= second && second <= third && third <= fourth in
                      let two = second <= first && first <= third && third <= fourth in
                      one || two && (truncate e.gelevel) <= (truncate e3.gelevel)
                    )
                    else (
                      let first = min a b and
                          second = min c d and
                          third = max a b and
                          fourth = max c d in
                      let one = first <= second && second <= third && third <= fourth in
                      let two = second <= first && first <= fourth && fourth <= third in
                      one || two && (truncate e.gelevel) <= (truncate e3.gelevel)
                    )
                  ) else (
                    false
                  )
                | _ -> false
              ) graph.edges in
            Edges.iter (fun e4 ->
              match e4 with
                | Group_edge e4 ->
                  e4.gelevel <- e4.gelevel +. 1.
                | _ ->()
            ) to_re_evaluate;
          );
        (* e.gelabel <- sprintf "%f" e.gelevel;*)
        | Word_edge _ -> ()
      ) edges2;

    let edges3 = Dep2pict_types.tranform_edges_to_edges3 graph in
    Edges3.iter (
      fun e -> match e with
        | Group_edge e ->
          if (e.gelevel > 0.)
          then (
            let nodeA =
              try Dep2pict_types.get_word e.gefrom_node graph.nodes
              with Not_found -> Dep2pict_types.get_group e.gefrom_node graph.nodes in
            let nodeB =
              try Dep2pict_types.get_word e.geto_node graph.nodes
              with Not_found -> Dep2pict_types.get_group e.geto_node graph.nodes in
            let c = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in (* from *)
            let d = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in (* to *)
            let efrom = min c d in
            let eto = max c d in
            let need_down_step = ref true in
            Edges3.iter (
              fun e2 -> match e2 with
                | Group_edge e2 ->
                  if (e2.geindex != e.geindex && e2.gelevel < 1.)
                  then (
                    let nodeA =
                      try Dep2pict_types.get_word e2.gefrom_node graph.nodes
                      with Not_found -> Dep2pict_types.get_group e2.gefrom_node graph.nodes in
                    let nodeB =
                      try Dep2pict_types.get_word e2.geto_node graph.nodes
                      with Not_found ->  Dep2pict_types.get_group e2.geto_node graph.nodes in
                    let a = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in (* from *)
                    let b = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in (* to *)
                    if (abs(a-b) <= abs(c-d)) then (
                      let e2from = min a b in
                      let e2to = max a b in
                      if (efrom <= e2from)
                      then (
                        if (eto >= e2to)
                        then need_down_step := false
                      )
                    )
                  )
                | _ -> ()
            ) edges3;

            Edges3.iter (
              fun e2 -> match e2 with
                | Group_edge e2 ->
                  if (e2.geindex != e.geindex)
                  then (
                    let nodeA =
                      try Dep2pict_types.get_word e2.gefrom_node graph.nodes
                      with Not_found -> Dep2pict_types.get_group e2.gefrom_node graph.nodes in
                    let nodeB =
                      try Dep2pict_types.get_word e2.geto_node graph.nodes
                      with Not_found ->  Dep2pict_types.get_group e2.geto_node graph.nodes in
                    let a = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in (* from *)
                    let b = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in (* to *)
                    if (c < d)
                    then ( (* premier arc -> *)
                      if (a > c && a < d && (b > d || b < c))
                      then need_down_step :=false
                      else
                        if (b > c && b < d && (a > d || a < c))
                        then need_down_step := false
                    ) else ( (* premier arc <- *)
                      if (a < c && a > d && (b < d || b > c))
                      then need_down_step := false
                      else
                        if (b < c && b > d && (a < d || a > c))
                        then need_down_step := false
                    );
                  )
                | _ -> ()
            ) edges3;
            if (!need_down_step)
            then e.gelevel <- e.gelevel -. 1.
          )
        | _ -> ()
    ) edges3;

    (* point de départ minimum des arcs *)
    let min_height = ref 0. in
    Nodes.iter
      (fun n -> match n with
        | Word word ->
          if (word.subword <> "")
          then (
            let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size word.subword in
            let splitted = protected_sharp_split (svg_escape word.subword) in
            if (List.length splitted > 1)
            then (min_height := max !min_height (word.wy+.(float_of_int (List.length splitted))*.(h+.2.)+.10.))
            else (min_height := max !min_height (word.wy+.h+.10.))
          )
          else (min_height := max !min_height (word.wy+.10.))
        | Group g -> min_height := max !min_height (g.gy+.10.);
      ) graph.nodes;

    min_height := !min_height -. 5.;

    (* decalage des arcs sur les noeuds + points *)
    Edges2.iter (
      fun e -> match e with
        | Word_edge _ -> ()
        | Group_edge e ->
          let nodeA =
            try Dep2pict_types.get_word e.gefrom_node graph.nodes
            with Not_found -> Dep2pict_types.get_group e.gefrom_node graph.nodes in
          let nodeB =
            try Dep2pict_types.get_word e.geto_node graph.nodes
            with Not_found -> Dep2pict_types.get_group e.geto_node graph.nodes in
          let efrom = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
          let eto = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in

          (* si arc positif -> *)
          if (efrom < eto)
          then (
            (* decalage à droite pour le noeud d'origine *)
            let tmp = Edges.filter
              (fun ee -> match ee with
                | Group_edge ee ->
                  let nodeA =
                    try Dep2pict_types.get_word ee.gefrom_node graph.nodes
                    with Not_found -> Dep2pict_types.get_group ee.gefrom_node graph.nodes in
                  let nodeB =
                    try Dep2pict_types.get_word ee.geto_node graph.nodes
                    with Not_found -> Dep2pict_types.get_group ee.geto_node graph.nodes in
                  let eefrom = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
                  let eeto = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in
                  ee.gelevel > e.gelevel && ((eefrom = efrom && eeto > efrom) || (eeto = efrom && eefrom > efrom))
                | _ -> false
              ) graph.edges in
            let tmp = float_of_int (Edges.cardinal tmp) in

            (* decalage à gauche pour le noeud d'arrivée *)
            let tmp2 = Edges.filter
              (fun ee -> match ee with
                | Group_edge ee ->
                  let nodeA =
                    try Dep2pict_types.get_word ee.gefrom_node graph.nodes
                    with Not_found -> Dep2pict_types.get_group ee.gefrom_node graph.nodes in
                  let nodeB =
                    try Dep2pict_types.get_word ee.geto_node graph.nodes
                    with Not_found -> Dep2pict_types.get_group ee.geto_node graph.nodes in
                  let eefrom = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
                  let eeto = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in
                  ee.gelevel > e.gelevel && ((eefrom = eto && eeto < eto) || (eeto = eto && eefrom < eto))
                | _ -> false
              ) graph.edges in
            let tmp2 = float_of_int (Edges.cardinal tmp2 +1) in
            begin match nodeA,nodeB with
              | Group w1, Word w2 ->
                e.gex1 <- w1.gx+.(w1.gwidth/.2.)+.graph.hspace*.tmp;
                e.gex2 <- w1.gx+.(w1.gwidth/.2.)+.graph.hspace*.tmp;
                e.gex3 <- w2.wx+.(w2.wwidth/.2.)-.graph.hspace*.tmp2;
                e.gex4 <- w2.wx+.(w2.wwidth/.2.)-.graph.hspace*.tmp2;
                e.gey1 <- w1.gy-.w1.gheight+.1.;
                e.gey2 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey3 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey4 <-
                  if (w2.subword <> "")
                  then (
                    let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size w2.subword in
                    let splitted = protected_sharp_split w2.subword in
                    if (List.length splitted > 1)
                    then (w2.wy+.(float_of_int (List.length splitted))*.(h+.2.)+.3.)
                    else (w2.wy+.h+.4.)
                  ) else (w2.wy+.4.)
              | Word w1, Group w2 ->
                e.gex1 <- w1.wx+.(w1.wwidth/.2.)+.graph.hspace*.tmp;
                e.gex2 <- w1.wx+.(w1.wwidth/.2.)+.graph.hspace*.tmp;
                e.gex3 <- w2.gx+.(w2.gwidth/.2.)-.graph.hspace*.tmp2;
                e.gex4 <- w2.gx+.(w2.gwidth/.2.)-.graph.hspace*.tmp2;
                e.gey1 <-
                  if (w1.subword <> "")
                  then (
                    let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size w1.subword in
                    let splitted = protected_sharp_split w1.subword in
                    if (List.length splitted > 1)
                    then (w1.wy+.(float_of_int (List.length splitted))*.(h+.2.)+.1.)
                    else (w1.wy+.h+.3.)
                  )
                  else (w1.wy+.3.);
                e.gey2 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey3 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey4 <- w2.gy-.w2.gheight+.3.;
              | Group w1, Group w2 ->
                e.gex1 <- w1.gx+.(w1.gwidth/.2.)+.graph.hspace*.tmp;
                e.gex2 <- w1.gx+.(w1.gwidth/.2.)+.graph.hspace*.tmp;
                e.gex3 <- w2.gx+.(w2.gwidth/.2.)-.graph.hspace*.tmp2;
                e.gex4 <- w2.gx+.(w2.gwidth/.2.)-.graph.hspace*.tmp2;
                e.gey1 <- w1.gy-.w1.gheight+.1.;
                e.gey2 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey3 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey4 <- w2.gy-.w2.gheight+.3.;
              | Word w1, Word w2 ->
                e.gex1 <- w1.wx+.(w1.wwidth/.2.)+.graph.hspace*.tmp;
                e.gex2 <- w1.wx+.(w1.wwidth/.2.)+.graph.hspace*.tmp;
                e.gex3 <- w2.wx+.(w2.wwidth/.2.)-.graph.hspace*.tmp2;
                e.gex4 <- w2.wx+.(w2.wwidth/.2.)-.graph.hspace*.tmp2;
                e.gey1 <-
                  if (w1.subword <> "")
                  then (
                    let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size w1.subword in
                    let splitted = protected_sharp_split w1.subword in
                    if (List.length splitted > 1)
                    then (w1.wy+.(float_of_int (List.length splitted))*.(h+.2.)+.3.)
                    else (w1.wy+.h+.3.)
                  ) else (w1.wy+.3.;);
                e.gey2 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey3 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey4 <-
                  if (w2.subword <> "")
                  then (
                    let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size w2.subword in
                    let splitted = protected_sharp_split w2.subword in
                    if (List.length splitted > 1)
                    then (w2.wy+.(float_of_int (List.length splitted))*.(h+.2.)+.3.)
                    else (w2.wy+.h+.4.)
                  ) else (w2.wy+.4.);
            end;

          )
          else ( (* arc negatif <- *)
            (* decalage à gauche pour le noeud d'origine *)
            let tmp = Edges.filter (fun ee -> match ee with
              | Group_edge ee ->
                let nodeA =
                  try Dep2pict_types.get_word ee.gefrom_node graph.nodes
                  with Not_found -> Dep2pict_types.get_group ee.gefrom_node graph.nodes in
                let nodeB =
                  try Dep2pict_types.get_word ee.geto_node graph.nodes
                  with Not_found -> Dep2pict_types.get_group ee.geto_node graph.nodes in
                let eefrom = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
                let eeto = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in
                ee.gelevel > e.gelevel && ((efrom = eefrom && eeto < efrom) || (eefrom < efrom && efrom = eeto))
              | _ -> false
            ) graph.edges in
            let tmp = float_of_int (Edges.cardinal tmp +1) in
            (* decalage à droite pour le noeud d'arrivée *)
            let tmp2 = Edges.filter (fun ee -> match ee with
              | Group_edge ee ->
                let nodeA =
                  try Dep2pict_types.get_word ee.gefrom_node graph.nodes
                  with Not_found -> Dep2pict_types.get_group ee.gefrom_node graph.nodes in
                let nodeB =
                  try Dep2pict_types.get_word ee.geto_node graph.nodes
                  with Not_found -> Dep2pict_types.get_group ee.geto_node graph.nodes in
                let eefrom = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
                let eeto = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in
                ee.gelevel > e.gelevel && ((eefrom = eto && eeto > eto) || (eeto = eto && eefrom > eto) )
              | _ -> false
            ) graph.edges in
            let tmp2 = float_of_int (Edges.cardinal tmp2) in
            begin match nodeA,nodeB with
              | Group w1, Word w2 ->
                e.gex1 <- w1.gx+.(w1.gwidth/.2.)-.graph.hspace*.tmp;
                e.gex2 <- w1.gx+.(w1.gwidth/.2.)-.graph.hspace*.tmp;
                e.gex3 <- w2.wx+.(w2.wwidth/.2.)+.graph.hspace*.tmp2;
                e.gex4 <- w2.wx+.(w2.wwidth/.2.)+.graph.hspace*.tmp2;
                e.gey1 <- w1.gy-.w1.gheight+.1.;

                e.gey2 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey3 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey4 <-
                  if (w2.subword <> "")
                  then (
                    let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size w2.subword in
                    let splitted = protected_sharp_split w2.subword in
                    if (List.length splitted > 1)
                    then (w2.wy+.(float_of_int (List.length splitted))*.(h+.2.)+.3.)
                    else (w2.wy+.h+.4.)
                  ) else (w2.wy+.4.;)
              | Word w1, Group w2 ->
                e.gex1 <- w1.wx+.(w1.wwidth/.2.)-.graph.hspace*.tmp;
                e.gex2 <- w1.wx+.(w1.wwidth/.2.)-.graph.hspace*.tmp;
                e.gex3 <- w2.gx+.(w2.gwidth/.2.)+.graph.hspace*.tmp2;
                e.gex4 <- w2.gx+.(w2.gwidth/.2.)+.graph.hspace*.tmp2;
                e.gey1 <-
                  if (w1.subword <> "")
                  then  (
                    let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size w1.subword in
                    let splitted = protected_sharp_split w1.subword in
                    if (List.length splitted > 1)
                    then (w1.wy+.(float_of_int (List.length splitted))*.(h+.2.)+.2.)
                    else (w1.wy+.h+.3.)
                  )
                  else (w1.wy+.3.);
                e.gey2 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey3 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey4 <- w2.gy-.w2.gheight+.3.;
              | Group w1, Group w2 ->
                e.gex1 <- w1.gx+.(w1.gwidth/.2.)-.graph.hspace*.tmp;
                e.gex2 <- w1.gx+.(w1.gwidth/.2.)-.graph.hspace*.tmp;
                e.gex3 <- w2.gx+.(w2.gwidth/.2.)+.graph.hspace*.tmp2;
                e.gex4 <- w2.gx+.(w2.gwidth/.2.)+.graph.hspace*.tmp2;
                e.gey1 <- w1.gy-.w1.gheight+.1.;
                e.gey2 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey3 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey4 <- w2.gy-.w2.gheight+.3.
              | Word w1, Word w2 ->
                e.gex1 <- w1.wx+.(w1.wwidth/.2.)-.graph.hspace*.tmp;
                e.gex2 <- w1.wx+.(w1.wwidth/.2.)-.graph.hspace*.tmp;
                e.gex3 <- w2.wx+.(w2.wwidth/.2.)+.graph.hspace*.tmp2;
                e.gex4 <- w2.wx+.(w2.wwidth/.2.)+.graph.hspace*.tmp2;
                e.gey1 <-
                  if (w1.subword <> "")
                  then  (
                    let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size w1.subword in
                    let splitted = protected_sharp_split w1.subword in
                    if (List.length splitted > 1)
                    then (w1.wy+.(float_of_int (List.length splitted))*.(h+.2.)+.3.)
                    else (w1.wy+.h+.3.)
                  ) else (w1.wy+.3.);
                e.gey2 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey3 <- !min_height+.graph.vspace*.e.gelevel;
                e.gey4 <-
                  if (w2.subword <> "")
                  then (
                    let (_,h,_) = Font_utilities.get_text_dimension graph.fontname graph.subword_size w2.subword in
                    let splitted = protected_sharp_split w2.subword in
                    if (List.length splitted > 1)
                    then (w2.wy+.(float_of_int (List.length splitted))*.(h+.2.)+.3.)
                    else (w2.wy+.h+.4.)
                  )
                  else (w2.wy+.4.;)
            end
          )
    ) edges2;

    (* recentrage des arcs sur les mots *)
    Edges2.iter (
      fun e -> match e with
        | Word_edge _ -> ()
        | Group_edge e ->
          let nodeA =
            try Dep2pict_types.get_word e.gefrom_node graph.nodes
            with Not_found -> Dep2pict_types.get_group e.gefrom_node graph.nodes in
          let nodeB =
            try Dep2pict_types.get_word e.geto_node graph.nodes
            with Not_found -> Dep2pict_types.get_group e.geto_node graph.nodes in
          let efrom = match nodeA with Word w -> w.windex_x | Group g -> g.gindex_x in
          let eto = match nodeB with Word w -> w.windex_x | Group g -> g.gindex_x in

          begin match nodeA,nodeB with
            | Word w1, Group w2 ->
              let diff1 = w1.wnb_edges_right_bottom - w1.wnb_edges_left_bottom - 1 in
              if (diff1 > 0)
              then (
                let dec = (foi (w1.wnb_edges_right_bottom - w1.wnb_edges_left_bottom)) /. 2. -. 0.5 in
                e.gex1 <- e.gex1 -. (dec*.graph.hspace);
                e.gex2 <- e.gex2 -. (dec*.graph.hspace);
              )
              else
                if (diff1 <= -1)
                then (
                  let dec = (foi (w1.wnb_edges_left_bottom - w1.wnb_edges_right_bottom)) /. 2. +. 0.5 in
                  e.gex1 <- e.gex1 +. (dec*.graph.hspace);
                  e.gex2 <- e.gex2 +. (dec*.graph.hspace);
                );

              let diff2 = w2.gnb_edges_right - w2.gnb_edges_left - 1 in
              if (diff2 > 0)
              then (
                let dec = (foi (w2.gnb_edges_right - w2.gnb_edges_left)) /. 2. -. 0.5 in
                e.gex3 <- e.gex3 -. (dec*.graph.hspace);
                e.gex4 <- e.gex4 -. (dec*.graph.hspace);
              ) else
                if (diff2 <= -1)
                then (
                  let dec = (foi (w2.gnb_edges_left - w2.gnb_edges_right)) /. 2. +. 0.5 in
                  e.gex3 <- e.gex3 +. (dec*.graph.hspace);
                  e.gex4 <- e.gex4 +. (dec*.graph.hspace);
                );
            | Group g, Word w ->
              let diff1 = g.gnb_edges_right - g.gnb_edges_left - 1 in
              if (diff1 > 0)
              then (
                let dec = (foi (g.gnb_edges_right - g.gnb_edges_left)) /. 2. -. 0.5 in
                e.gex1 <- e.gex1 -. (dec*.graph.hspace);
                e.gex2 <- e.gex2 -. (dec*.graph.hspace);
              )
              else
                if (diff1 <= -1)
                then (
                  let dec = (foi (g.gnb_edges_left - g.gnb_edges_right)) /. 2. +. 0.5 in
                  e.gex1 <- e.gex1 +. (dec*.graph.hspace);
                  e.gex2 <- e.gex2 +. (dec*.graph.hspace);
                );
              let diff2 = w.wnb_edges_right_bottom - w.wnb_edges_left_bottom - 1 in
              if (diff2 > 0)
              then (
                let dec = (foi (w.wnb_edges_right_bottom - w.wnb_edges_left_bottom)) /. 2. -. 0.5 in
                e.gex3 <- e.gex3 -. (dec*.graph.hspace);
                e.gex4 <- e.gex4 -. (dec*.graph.hspace);
              )
              else
                if (diff2 <= -1)
                then (
                  let dec = (foi (w.wnb_edges_left_bottom - w.wnb_edges_right_bottom)) /. 2. +. 0.5 in
                  e.gex3 <- e.gex3 +. (dec*.graph.hspace);
                  e.gex4 <- e.gex4 +. (dec*.graph.hspace);
                );
            | Group g1, Group g2 ->
              let diff1 = g1.gnb_edges_right - g1.gnb_edges_left - 1 in
              if (diff1 > 0)
              then (
                let dec = (foi (g1.gnb_edges_right - g1.gnb_edges_left)) /. 2. -. 0.5 in
                e.gex1 <- e.gex1 -. (dec*.graph.hspace);
                e.gex2 <- e.gex2 -. (dec*.graph.hspace);
              )
              else
                if (diff1 <= -1)
                then (
                  let dec = (foi (g1.gnb_edges_left - g1.gnb_edges_right)) /. 2. +. 0.5 in
                  e.gex1 <- e.gex1 +. (dec*.graph.hspace);
                  e.gex2 <- e.gex2 +. (dec*.graph.hspace);
                );
              let diff2 = g2.gnb_edges_right - g2.gnb_edges_left - 1 in
              if (diff2 > 0)
              then (
                let dec = (foi (g2.gnb_edges_right - g2.gnb_edges_left)) /. 2. -. 0.5 in
                e.gex3 <- e.gex3 -. (dec*.graph.hspace);
                e.gex4 <- e.gex4 -. (dec*.graph.hspace);
              )
              else
                if (diff2 <= -1)
                then (
                  let dec = (foi (g2.gnb_edges_left - g2.gnb_edges_right)) /. 2. +. 0.5 in
                  e.gex3 <- e.gex3 +. (dec*.graph.hspace);
                  e.gex4 <- e.gex4 +. (dec*.graph.hspace);
                );
            | Word w1, Word w2 ->
              let diff1 = w1.wnb_edges_right_bottom - w1.wnb_edges_left_bottom - 1 in
              if (diff1 > 0)
              then (
                let dec = (foi (w1.wnb_edges_right_bottom - w1.wnb_edges_left_bottom)) /. 2. -. 0.5 in
                e.gex1 <- e.gex1 -. (dec*.graph.hspace);
                e.gex2 <- e.gex2 -. (dec*.graph.hspace);
              )
              else
                if (diff1 <= -1)
                then (
                  let dec = (foi (w1.wnb_edges_left_bottom - w1.wnb_edges_right_bottom)) /. 2. +. 0.5 in
                  e.gex1 <- e.gex1 +. (dec*.graph.hspace);
                  e.gex2 <- e.gex2 +. (dec*.graph.hspace);
                );
              let diff2 = w2.wnb_edges_right_bottom - w2.wnb_edges_left_bottom - 1 in
              if (diff2 > 0)
              then (
                let dec = (foi (w2.wnb_edges_right_bottom - w2.wnb_edges_left_bottom)) /. 2. -. 0.5 in
                e.gex3 <- e.gex3 -. (dec*.graph.hspace);
                e.gex4 <- e.gex4 -. (dec*.graph.hspace);
              )
              else
                if (diff2 <= -1)
                then (
                  let dec = (foi (w2.wnb_edges_left_bottom - w2.wnb_edges_right_bottom)) /. 2. +. 0.5 in
                  e.gex3 <- e.gex3 +. (dec*.graph.hspace);
                  e.gex4 <- e.gex4 +. (dec*.graph.hspace);
                );
          end;
          let sign = if (efrom < eto) then 1. else -.1. in
          bprintf buff "<marker id=\"markerendgroup_%s\" markerWidth=\"5\" markerHeight=\"5\" markerUnits=\"userSpaceOnUse\" orient=\"auto\" refX=\"2\" refY=\"2\">\n" (soi e.geindex);
          bprintf buff "<path d=\"M0,0 l4,2 l-4,2 z\" style=\"fill:%s;\"/>\n" e.gecolor;
          bprintf buff "</marker>\n";
          let label_id = sprintf "label__%s" e.gedge_id in
          let path_id = sprintf "path__%s" e.gedge_id in
          if (e.gelabel<>"")
          then
            begin
              let (width,ascent,descent) = Font_utilities.get_text_dimension graph.fontname graph.edge_label_size e.gelabel in
              let pad = 2. in
              let x = match e.gelabel_align with
                | 0 -> float_of_int (abs(truncate(e.gex3-.e.gex2)))/.2.+.(min e.gex3 e.gex2)+.e.gelabel_offset_pos-.e.gelabel_offset_neg
                | 1 -> e.gex3
                | 2 -> e.gex1
                | _ -> float_of_int (abs(truncate(e.gex3-.e.gex2)))/.2.+.(min e.gex3 e.gex2)+.e.gelabel_offset_pos-.e.gelabel_offset_neg in
              let y = e.gey2+.ascent+.8. in

              (match e.gebgcolor with
                | "transparent" -> ()
                | color ->
                  update_leftmost_highlight x;
                  bprintf buff "<rect x=\"%s\" y=\"%s\" width=\"%s\" height=\"%s\" style=\"fill:%s;fill-opacity:100;\" />\n"
                    (transform_float (x-.width/.2. -. pad))
                    (transform_float (y-.ascent -. pad))
                    (transform_float (width +. 2. *. pad))
                    (transform_float (ascent+.descent +. 2. *. pad))
                    color
              );

              bprintf buff "<text id=\"%s\" x=\"%s\" y=\"%s\" style=\"fill:%s;font-size:%dpx;font-family:%s;text-anchor:%s;text-align:%s\" %s>%s%s</text>\n"
                label_id (transform_float x) (transform_float y)
                e.geforecolor
                graph.edge_label_size
                graph.fontname
                (match e.gelabel_align with 1 -> "end" | 2 -> "start" | _ -> "middle")
                (match e.gelabel_align with 1 -> "right" | 2 -> "left" | _ -> "center")
                (omo ())
                (title label_id)
                (svg_escape e.gelabel)
            end;

          bprintf buff "<path style=\"%s%s\" stroke=\"%s\" fill=\"none\" d=\"M %s,%s %s,%s A 3 3 0 0 %s %s,%s L %s,%s A 3 3 0 0 %s %s,%s L %s,%s\" %s>%s</path>\n"
            (if e.georiented then sprintf "marker-end:url(#markerendgroup_%i);" e.geindex else "")
            (match e.gestyle with
              | "dot" -> "stroke-dasharray:1,1;"
              | "dash" -> "stroke-dasharray:3,1;"
              | _ -> "")
            e.gecolor
            (transform_float e.gex1) (transform_float e.gey1)
            (transform_float e.gex2) (transform_float (e.gey2+.4.))
            (if e.gex3 < e.gex2 then "1" else "0")
            (transform_float (e.gex2+.(3.*.sign))) (transform_float (e.gey2+.7.))
            (transform_float (e.gex3-.(3.*.sign))) (transform_float (e.gey3+.7.))
            (if e.gex3 < e.gex2 then "1" else "0")
            (transform_float e.gex3 ) (transform_float (e.gey3+.4.))
            (transform_float e.gex4) (transform_float e.gey4)
            (omo ())
            (title path_id)
    ) edges2;
    Buffer.contents buff

  (* ----------------------------------------------------------------------------- *)
  let graph_to_svg graph =
    leftmost_highlight := None; (* reinitialize *)
    let nodes = nodes_to_svg graph in
    let edges = words_edges_to_svg graph in
    words_edges_to_svg_bottom graph;
    let edges2 = groups_edges_to_svg graph in
    let (height,width,trans_y,trans_x) = get_dimension graph in
    let scale = (float_of_int graph.scale) /. 100. in
    let buff = Buffer.create 32 in
    bprintf buff "<?xml version=\"1.0\" standalone=\"no\"?>\n";
    bprintf buff "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n";
    bprintf buff "\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n";
    bprintf buff "<!-- Created with dep2pict -->\n";
    bprintf buff "<svg xmlns:svg=\"http://www.w3.org/2000/svg\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"%s\" height=\"%s\">\n"
      (transform_float(width*.scale)) (transform_float(height*.scale +. 2.));

    bprintf buff "<rect x=\"0\" y=\"0\" width=\"%s\" height=\"%s\" style=\"fill:%s;fill-opacity:%s;\" />\n"
      (transform_float(width*.scale)) (transform_float(height*.scale +. 2.)) (graph.background) (transform_float graph.opacity);
    bprintf buff "<g  transform=\"translate(%s,%s) scale(%s,%s)\">\n"
      (transform_float(trans_x*.scale)) (transform_float(trans_y*.scale)) (transform_float scale) (transform_float scale );
    bprintf buff "%s\n" nodes;
    bprintf buff "%s\n" edges;
    bprintf buff "%s\n" edges2;
    bprintf buff "</g>\n";
    bprintf buff "</svg>\n";
    (Buffer.contents buff, (match !leftmost_highlight with None -> None | Some v -> Some (v *. scale)))



  (* ----------------------------------------------------------------------------- *)
  (* Exported types and functions *)
  (* ----------------------------------------------------------------------------- *)

  type t = Dep2pict_types.graph

  let shift = ref None

  let highlight_shift () = !shift

  let from_dep ?(rtl=false) dep =
    handle (fun () ->
      Dep2pict_global.reinit();
      first_level_top_to_add := 0.;
      let lexbuf = Lexing.from_string dep in
      let graph = Deppars.main Deplex.main lexbuf in
      if rtl then graph_reverse graph;
      nodes_placement graph;
      graph
    )

  let load_dep ?(rtl=false) filename =
    let bytes = (Dep2pict_utils.read_file filename) in
    from_dep ~rtl (Bytes.to_string bytes)

  let to_svg graph = let (svg,new_shift) = graph_to_svg graph in
    shift := new_shift;
    svg

  let save_svg ~filename graph = Dep2pict_utils.save ~filename (to_svg graph)

  let save_pdf ~filename graph =
    let tmp_name = Filename.temp_file "tmp" "svg" in
    save_svg ~filename:tmp_name graph;
    match Sys.command ("rsvg-convert -f pdf -o "^filename^" "^tmp_name) with
    | 0 -> ()
    | _ -> error "rsvg-convert command is not available"

  let save_png ~filename graph =
    let tmp_name = Filename.temp_file "tmp" "svg" in
    save_svg ~filename:tmp_name graph;
    match Sys.command ("rsvg-convert -f png -o "^filename^" "^tmp_name) with
    | 0 -> ()
    | _ -> error "rsvg-convert command is not available"

end
