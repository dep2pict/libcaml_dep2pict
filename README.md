# Dep2pict

Dep2pict is an ocaml library which aim is to draw dependency syntax structures.

See [Doc](http://dep2pict.loria.fr) for more information.
