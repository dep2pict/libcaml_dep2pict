{
(** Lexeur de fichier au format .dep *)
open Deppars
open Dep2pict_global
}

let integer = ['1'-'9']*['0'-'9']+
let float_ = ['1'-'9']*['0'-'9']+'.'['0'-'9']*
let value = [^'\t''\n'' '';''\"''=''{''}''['']']+
let word = [^'\t''\n''>''"''{''}']+

rule string_lex target = parse
  | '\\'    { if !Dep2pict_global.escaped 
              then (Dep2pict_global.tmp_string := !Dep2pict_global.tmp_string^"\\");
              Dep2pict_global.escaped := not !Dep2pict_global.escaped; 
              string_lex target lexbuf
            }
  | '\n'    { incr Dep2pict_global.line; Lexing.new_line lexbuf; Dep2pict_global.tmp_string := !Dep2pict_global.tmp_string^"\n"; string_lex target lexbuf }
  | '\"'    { if !Dep2pict_global.escaped then (Dep2pict_global.tmp_string := !Dep2pict_global.tmp_string^"\"";  Dep2pict_global.escaped := false; string_lex target lexbuf) else ( QUOTED_STRING(!Dep2pict_global.tmp_string) ) }
  | _ as c  { Dep2pict_global.escaped := false; Dep2pict_global.tmp_string := !Dep2pict_global.tmp_string^(Printf.sprintf "%c" c); string_lex target lexbuf }

and main = parse
  | ['\n']               { incr Dep2pict_global.line ; main lexbuf}
  | [ '\t' ' ' '\r']     { main lexbuf}

  | "\""                 { tmp_string := ""; string_lex main lexbuf }

  | "{"                  { LEFT_BRACKET}
  | "}"                  { RIGHT_BRACKET }
  | "["                  { LEFT_CROC}
  | "]"                  { RIGHT_CROC }
  | "="                  { EQUAL }
  | ";"                  { SEMI }
  | "->"                 { ARROW }
  | ".."                 { DDOT }

  | "[GRAPH]"            { GRAPH }
  | "[GR_GRAPH]"         { GR_GRAPH }
  | "[WORDS]"            { WORDS }
  | "[EDGES]"            { EDGES }
  | "[GROUPS]"           { GROUPS }
  
  | "background"         { BACKGROUND }
  | "word_size"          { WORD_SIZE }
  | "subword_size"       { SUBWORD_SIZE }
  | "subgroup_size"      { SUBGROUP_SIZE }
  | "supgroup_size"      { SUPGROUP_SIZE }
  | "edge_label_size"    { EDGELABEL_SIZE }
  | "words"              { WORDS_ATT }
  | "word"               { MAINWORD }
  | "subword"            { SUBWORD }
  | "supword"            { SUPWORD }
  | "forecolor"          { FORECOLOR }
  | "color"              { COLOR }
  | "bgcolor"            { BGCOLOR }
  | "style"              { STYLE }
  | "hspace"             { HSPACE }
  | "vspace"             { VSPACE }
  | "unoriented"         { UNORIENTED }
  | "subcolor"           { SUBCOLOR }
  | "italic"             { ITALIC }
  | "bold"               { BOLD }
  | "label_align"        { ALIGN }
  | "scale"              { SCALE }
  | "label_offset_pos"   { LABEL_OFFSET_POS}
  | "label_offset_neg"   { LABEL_OFFSET_NEG}
  | "label"              { LABEL }
  | "opacity"            { OPACITY }
  | "fontname"           { FONTNAME }
  | "word_size"          { WORD_SIZE }
  | "word_spacing"       { WORD_SPACING }
  | "margin_top"         { MARGIN_TOP}
  | "margin_bottom"      { MARGIN_BOTTOM}
  | "margin_left"        { MARGIN_LEFT}
  | "margin_right"       { MARGIN_RIGHT}
  | "bordercolor"        { BORDER_COLOR }
  | "top"                { TOP }
  | "bottom"             { BOTTOM }
  | "left"               { LEFT }
  | "right"              { RIGHT }
      
  | integer as v         { INT(v) }
  | value as v           { VALUE(v) }
       
  | eof                  { EOF }

