open Dep2pict

let _ = 

(*	let m = Mutex.create () in*)
	
  let compute num = 
    Printf.printf "TEST %d : fromDepFileToSvgFile\n%!" num;
    begin
      try
	let (_,_) = Dep2pict.fromDepFileToSvgFile ("test"^(string_of_int num)^".dep") ("test"^(string_of_int num)^".svg") in
	Printf.printf "TEST %d : OK\n%!" num;
      with Dep2pict.Parse_error msgs -> 
	Printf.printf "TEST %d : FAIL\n%!" num;
	List.iter (fun (line,msg) -> Printf.eprintf "line : %d : %s<br/>%!" line msg) msgs;
    end;
  in
  for i = 1 to 9 do compute i done

	
