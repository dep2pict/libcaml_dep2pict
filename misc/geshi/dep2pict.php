<?php
/*************************************************************************************
 * dep2pict.php
 * -------
 * Author: Paul MASSON (paul.masson@inria.fr)
 * Release Version: 1.0.0.0
 * Date Started: 2010/05/27
 *
 * Dep2pict language file for GeSHi.
 ************************************************************************************/

$language_data = array (
    'LANG_NAME' => 'dep2pict',
    'COMMENT_SINGLE' => array(),
    'COMMENT_MULTI' => array(),
    'COMMENT_REGEXP' => array(),
    'CASE_KEYWORDS' => GESHI_CAPS_NO_CHANGE,
    'QUOTEMARKS' => array('"', "'"),
    'ESCAPE_CHAR' => '',
    'KEYWORDS' => array(
        1 => array(
            'background','word_size','subword_size','edgelabel_size','subword','forecolor',
            'label','color','style','hspace','vspace','oriented','subcolor','italic','bold','fromform',
            'toform','label_align','label_offset_pos','label_offset_neg'
            ),
        2 => array(
            'fuchsia', 'gray', 'green', 'maroon', 'navy', 'olive', 'purple',
            'red', 'white', 'yellow', 'pink', 'violet', 'blue', 'brown', 'black',
            'rect','ball','solid','dashed','dot','input'
            ),
        3 => array('GRAPH','WORDS','TOP_EDGES','BOTTOM_EDGES')
        ),
    'SYMBOLS' => array(
        '{', '}',';','-','=','>'
        ),
    'CASE_SENSITIVE' => array(
        GESHI_COMMENTS => false,
        1 => true,
        2 => true
        ),
    'STYLES' => array(
        'KEYWORDS' => array(
            1 => 'color: #00FF00;',
            2 => 'color: #FF0000;',
            3 => 'color: yellow;'
            ),
        'COMMENTS' => array(),
        'ESCAPE_CHAR' => array(),
        'BRACKETS' => array(
            0 => 'color: #00AA00;'
            ),
        'STRINGS' => array(
            0 => 'color: #ff0000;'
            ),
        'NUMBERS' => array(
            0 => 'color: #cc66cc;'
            ),
        'METHODS' => array(
            ),
        'SYMBOLS' => array(
            0 => 'color: #00AA00;'
            ),
        'SCRIPT' => array(
            ),
        'REGEXPS' => array(
            0 => 'color: #cc00cc;'
            )
        ),
    'URLS' => array(
        1 => '',
        2 => ''
        ),
    'OOLANG' => false,
    'OBJECT_SPLITTERS' => array(
        ),
    'REGEXPS' => array(
        0 => '\#[0-9][0-9][0-9][0-9][0-9][0-9]'
        ),
    'STRICT_MODE_APPLIES' => GESHI_NEVER,
    'SCRIPT_DELIMITERS' => array(
        ),
    'HIGHLIGHT_STRICT_BLOCK' => array(
        ),
    'TAB_WIDTH' => 4,
    'PARSER_CONTROL' => array(
        'KEYWORDS' => array(
            'DISALLOWED_AFTER' => '(?![a-zA-Z0-9_\|%\\-&\.])'
        )
    )
);

?>
