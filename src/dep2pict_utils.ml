
open Printf

let verbose = ref false
let verb msg =
  ksprintf (fun m -> if !verbose then printf "%s%!" m else ()) msg

let transform_float strf = 
	if (strf -. (float_of_int (int_of_float strf))) = 0. then
		let s = (sprintf "%.0f" strf) in
		s
	else
		Str.global_replace (Str.regexp ",") "." (sprintf "%.5f" strf)


(**
 Lecture d'un fichier texte
 @param path le chemin d'accès au fichier à lire
 @return une chaine de caractères représentant le contenu du fichier lu
 *)
let read_file path =
    let file = open_in path in
    let file_size = in_channel_length file in
    let output_string = Bytes.create file_size in
    really_input file output_string 0 file_size;
    close_in file;
    output_string

let read_lines file =
  let ch = open_in file in
  
  (* if the input file contains an UTF-8 byte order mark (EF BB BF), skip 3 bytes, else get back to 0 *)
  (match input_byte ch with 239 -> seek_in ch 3 | _ -> seek_in ch 0);

  let res = ref [] in
  let num = ref 0 in
  try
    while true do
      let next = input_line ch in 
      incr num;
      if next = "" 
      then ()
      else res := (!num, next) :: !res
    done; assert false
  with End_of_file -> close_in ch; List.rev !res

let save ~filename text =
  let out = open_out filename in
   output_string out text;
   close_out out

   
let abs_f f = if (f>0.) then f else -1.*.f

(**
 Reecriture de string_of_int
 *)
let soi i = string_of_int i;;

(**
 Reecriture de string_of_float
 *)
let sof f = string_of_float f;;


(**
 Reecriture de float_of_string
 *)
let fos s = float_of_string s;;


(**
 Reecriture de int_of_string
 *)
let ios s = int_of_string s;;

(**
 Reecriture de float_of_int
 *)
let foi i = float_of_int i;;

(**
 Reecriture de int_of_float
 *)
let iof i = int_of_float i;;

(**
 Reecriture de bool_of_string
 *)
let bos s = bool_of_string s;;
		
exception Cant_parse of ((int*string) list)
exception Cant_find_index of string
exception Id_already_in_use of string
exception Loop_in_dependencies of string
exception Syntax_error of string		
		
		
		
